﻿using UnityEngine;
using System.Collections;
//based on http://creatiosoft.com/forum/showthread.php?tid=335
public class OrthographicCameraPinchZoom: MonoBehaviour 
{
    // It sets the size of the camera at runtime.
	// after the fingers pinch in/out.
	float distance;
	// The max scale limit of the camera (how far away max)
	private float maxScale = 10f;
	// The min scale limit of the camera (how close by max)
	private float minScale = 5f;
	
	private float scaleSpeed = 0.1f; //between 0 and 1
	
	//freeq draught emitter
	private GameObject freeqDraught;
	private float freeqDraughtMaxY;
	private float freeqDraughtMinY;
	
	// Use this for initialization
	void Start () 
	{
		//Set the main camera's size to minScale.
		Camera.main.orthographicSize = minScale;
		freeqDraught = GameObject.Find("FreeqDraught");
		freeqDraughtMaxY = freeqDraught.transform.position.y;
		freeqDraughtMinY = freeqDraughtMaxY - ((maxScale - minScale) /2);
	}
	
	// Update is called once per frame
	void Update () {
		//If two fingers are touched on the display.
		if(Input.touchCount==2)
		{
			//Store the first and second touch
			Touch touch0 = Input.GetTouch(0);
			Touch touch1 = Input.GetTouch(1);
			
			//the current distance between the two fingers touched on the screen
			Vector3 currentDist = touch0.position - touch1.position;
			
			//Calculate the previous distance when the fingers were moved
			Vector3 prevDist = (touch0.position - touch0.deltaPosition) - (touch1.position-touch1.deltaPosition);
			
			//The magnitude of two vectors is to be calculated which gives a very large value. Dividing by an integer triggers the smoothness of the changing value of the delta.
			float delta = (currentDist.magnitude - prevDist.magnitude)*(scaleSpeed);
			
			//decreases the distance with factor. 
			distance-=delta;
			//limit the maximum and the minimum values 
			bool minReached = false;
			if(distance<minScale){
				distance = minScale;	
				minReached = true;
			}
			bool maxReached = false;
			if(distance>maxScale){
				distance = maxScale;
				maxReached = true;
			}
			
			// Change the size of the camera accordingly with pinch
			// which turns out to be a pinch/zoom effect
			Camera.main.orthographicSize = distance;
			
			//adjust particle emitter position
			if(!minReached && !maxReached)
				freeqDraught.transform.position += new Vector3(0, delta/2f, 0);
			else if(minReached)
				freeqDraught.transform.position = new Vector3(0, freeqDraughtMaxY, 0);
			else if(maxReached)
				freeqDraught.transform.position = new Vector3(0, freeqDraughtMinY, 0);
		}
	
	}
 
}
