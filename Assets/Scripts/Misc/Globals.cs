﻿using UnityEngine;
using System.Collections;

public static class Globals 
{
	//misc
	public static bool IsPause = false;
	public static bool AudioIsOn = true;
	
	
	//style
	public static GUISkin FreeqSkin = (GUISkin)Resources.Load("FreeqSkin");
	public static GUIStyle RectButtonStyle = FreeqSkin.GetStyle("Button_Rect");
	public static GUIStyle SquareButtonStyle = FreeqSkin.GetStyle("Button_Square");
	public static GUIStyle SquareButtonImageStyle = FreeqSkin.GetStyle("Button_Square_Image");
	public static GUIStyle SquareButtonItem = FreeqSkin.GetStyle("Button_Square_Item");
	public static GUIStyle SquareButtonItemSelected = FreeqSkin.GetStyle("Button_Square_Item_Selected");
	public static GUIStyle ObjectiveButtonStyle = FreeqSkin.GetStyle("Button_Objective");
	public static GUIStyle ObjectiveCompleteButtonStyle = FreeqSkin.GetStyle("Button_Objective_Complete");
	public static GUIStyle DialogStyle = FreeqSkin.GetStyle("Dialog_Back");
	public static GUIStyle DialogTextBackStyle = FreeqSkin.GetStyle("Dialog_Text_Back");
	public static GUIStyle DialogTextStyle = FreeqSkin.GetStyle("Dialog_Text");
	public static GUIStyle GameplayInfoMessageStyle = FreeqSkin.GetStyle("Gameplay_InfoMessage");
	public static GUIStyle ScoreTextStyle = FreeqSkin.GetStyle("Score_Text");
	public static GUIStyle ScoreTextCompleteStyle = FreeqSkin.GetStyle("Score_Text_Complete");
	public static GUIStyle ScoreTextCompleteSmallStyle = FreeqSkin.GetStyle("Score_Text_Complete_Small");
	public static GUIStyle TitleCompleteStyle = FreeqSkin.GetStyle("Title_Complete");
	public static GUIStyle FreeqInfoNumberLeftStyle = FreeqSkin.GetStyle("FreeqInfoNumber_left");
	public static GUIStyle FreeqInfoNumberRightStyle = FreeqSkin.GetStyle("FreeqInfoNumber_right");
	public static GUIStyle ParticleInfoNameLeftStyle = FreeqSkin.GetStyle("ParticleInfoLeftName");
	public static GUIStyle ParticleInfoNameRightStyle = FreeqSkin.GetStyle("ParticleInfoRightName");
	public static GUIStyle SubParticleInfoNumberStyle = FreeqSkin.GetStyle("SubParticleInfoNumber");
	public static GUIStyle SubParticleInfoNumberHoverStyle = FreeqSkin.GetStyle("SubParticleInfoNumberHover");
	public static float FontSize_Small = Screen.width/23;
	public static float FontSize_QuiteSmall = Screen.width/21;
	public static float FontSize_Medium = Screen.width/20;
	public static float FontSize_Big = Screen.width/18;
	public static float FontSize_BigMed = Screen.width/15;
	public static float FontSize_Bigger = Screen.width/12;
	public static float FontSize_Huge = Screen.width/10;
	
	//particle textures
	public static Texture2D PARTICLE_TEXTURE_MISSING = (Texture2D)Resources.Load("Textures/Particles/Highres/missing");
	
	public static Texture2D PARTICLE_TEXTURE_UQUARK = (Texture2D)Resources.Load("Textures/Particles/Highres/up_quark");
	public static Texture2D PARTICLE_TEXTURE_AUQUARK = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_up_quark");
	public static Texture2D PARTICLE_TEXTURE_DQUARK = (Texture2D)Resources.Load("Textures/Particles/Highres/down_quark");
	public static Texture2D PARTICLE_TEXTURE_ADQUARK = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_down_quark");
	public static Texture2D PARTICLE_TEXTURE_ELECTRON = (Texture2D)Resources.Load("Textures/Particles/Highres/electron");
	public static Texture2D PARTICLE_TEXTURE_AELECTRON = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_electron");
	public static Texture2D PARTICLE_TEXTURE_PROTON = (Texture2D)Resources.Load("Textures/Particles/Highres/proton");
	public static Texture2D PARTICLE_TEXTURE_APROTON = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_proton");
	public static Texture2D PARTICLE_TEXTURE_NEUTRON = (Texture2D)Resources.Load("Textures/Particles/Highres/neutron");
	public static Texture2D PARTICLE_TEXTURE_ANEUTRON = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_neutron");
	public static Texture2D PARTICLE_TEXTURE_HELIUM_NUCLEUS = (Texture2D)Resources.Load("Textures/Particles/Highres/helium_nucleus");
	public static Texture2D PARTICLE_TEXTURE_AHELIUM_NUCLEUS = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_helium_nucleus");
	public static Texture2D PARTICLE_TEXTURE_CARBON_NUCLEUS = (Texture2D)Resources.Load("Textures/Particles/Highres/carbon_nucleus");
	public static Texture2D PARTICLE_TEXTURE_ACARBON_NUCLEUS = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_carbon_nucleus");
	public static Texture2D PARTICLE_TEXTURE_HELIUM_ATOM = (Texture2D)Resources.Load("Textures/Particles/Highres/helium_atom");
	public static Texture2D PARTICLE_TEXTURE_AHELIUM_ATOM = (Texture2D)Resources.Load("Textures/Particles/Highres/anti_helium_atom");
	public static Texture2D PARTICLE_TEXTURE_CARBON_ATOM = (Texture2D)Resources.Load("Textures/Particles/Highres/carbon_atom");
	public static Texture2D PARTICLE_TEXTURE_ACARBON_ATOM= (Texture2D)Resources.Load("Textures/Particles/Highres/anti_carbon_atom");
	
	public static Texture2D MIXED_TEXTURE_UQUARK = (Texture2D)Resources.Load("Textures/Particles/Lowres/mix_upquark");
	public static Texture2D MIXED_TEXTURE_DQUARK = (Texture2D)Resources.Load("Textures/Particles/Lowres/mix_downquark");
	public static Texture2D MIXED_TEXTURE_ELECTRON = (Texture2D)Resources.Load("Textures/Particles/Lowres/mix_electron");
	public static Texture2D MIXED_TEXTURE_PROTON = (Texture2D)Resources.Load("Textures/Particles/Lowres/mix_proton");
	public static Texture2D MIXED_TEXTURE_NEUTRON = (Texture2D)Resources.Load("Textures/Particles/Lowres/mix_neutron");
	
	//button strings
	public static string STRING_LANGUAGE_BUTTON = "";
	public static string STRING_HELP_BUTTON = "";
	public static string STRING_AUDIO_BUTTON_ON = "";
	public static string STRING_AUDIO_BUTTON_OFF = "";
	public static string STRING_ABOUT_BUTTON = "";
	public static string STRING_RESET_BUTTON = "";
	public static string STRING_BACK_BUTTON = "";
	public static string STRING_SETTINGS_BUTTON = "";
	public static string STRING_PLAY_BUTTON = "";
	public static string STRING_QUIT_BUTTON = "";
	public static string STRING_YES_BUTTON = "";
	public static string STRING_NO_BUTTON = "";
	public static string STRING_CONFIRM_BUTTON = "";
	public static string STRING_PAUSE_BUTTON = "";
	public static string STRING_RESUME_BUTTON = "";
	public static string STRING_RESTART_BUTTON = "";
	public static string STRING_LEVELSELECT_BUTTON = "";
	public static string STRING_COMBINE_BUTTON = "";
	public static string STRING_RELEASE_BUTTON = "";
	
	//dialog strings
	public static string DIALOG_QUIT_TITLE = "";
	public static string DIALOG_RESET_TITLE = "";
	public static string DIALOG_HELP_TITLE = "";
	public static string DIALOG_ABOUT_TITLE = "";
	
	public static string DIALOG_QUIT_TEXT = "";
	public static string DIALOG_RESET_TEXT = "";
	public static string DIALOG_HELP_TEXT = "";
	public static string DIALOG_ABOUT_TEXT = "";
	
	//particle strings
	public static string PARTICLE_TEXT = "";
	public static string APARTICLE_TEXT = "";
	public static string PARTICLE_CREATED_TEXT = "";
	public static string PARTICLE_COMBINATIONERROR_TEXT = "";
	public static string PARTICLE_NOTSELECTED_TEXT = "";
	public static string ANTI_STRING = "";
	
	public static string PARTICLE_TEXT_UQUARK = "";
	public static string PARTICLE_TEXT_DQUARK = "";
	public static string PARTICLE_TEXT_ELECTRON = "";
	public static string PARTICLE_TEXT_PROTON = "";
	public static string PARTICLE_TEXT_NEUTRON = "";
	public static string PARTICLE_TEXT_HELIUMNUCLEUS = "";
	public static string PARTICLE_TEXT_HELIUMATOM = "";
	public static string PARTICLE_TEXT_CARBONNUCLEUS = "";
	public static string PARTICLE_TEXT_CARBONATOM = "";
	
	public static string PARTICLE_TEXT_AUQUARK = "";
	public static string PARTICLE_TEXT_ADQUARK = "";
	public static string PARTICLE_TEXT_AELECTRON = "";
	public static string PARTICLE_TEXT_APROTON = "";
	public static string PARTICLE_TEXT_ANEUTRON = "";
	public static string PARTICLE_TEXT_AHELIUMNUCLEUS = "";
	public static string PARTICLE_TEXT_AHELIUMATOM = "";
	public static string PARTICLE_TEXT_ACARBONNUCLEUS = "";
	public static string PARTICLE_TEXT_ACARBONATOM = "";
	
	public static string PARTICLE_TITLE_UQUARK = "";
	public static string PARTICLE_TITLE_DQUARK = "";
	public static string PARTICLE_TITLE_ELECTRON = "";
	public static string PARTICLE_TITLE_PROTON = "";
	public static string PARTICLE_TITLE_NEUTRON = "";
	public static string PARTICLE_TITLE_HELIUMNUCLEUS = "";
	public static string PARTICLE_TITLE_HELIUMATOM = "";
	public static string PARTICLE_TITLE_CARBONNUCLEUS = "";
	public static string PARTICLE_TITLE_CARBONATOM = "";
	
	public static string PARTICLE_TITLE_AUQUARK = "";
	public static string PARTICLE_TITLE_ADQUARK = "";
	public static string PARTICLE_TITLE_AELECTRON = "";
	public static string PARTICLE_TITLE_APROTON = "";
	public static string PARTICLE_TITLE_ANEUTRON = "";
	public static string PARTICLE_TITLE_AHELIUMNUCLEUS = "";
	public static string PARTICLE_TITLE_AHELIUMATOM = "";
	public static string PARTICLE_TITLE_ACARBONNUCLEUS = "";
	public static string PARTICLE_TITLE_ACARBONATOM = "";
	
	//level complete strings
	public static string COMPLETE_TEXT_CLEAR = "";
	public static string COMPLETE_TEXT_NOSTAR = "";
	public static string COMPLETE_TEXT_1STAR = "";
	public static string COMPLETE_TEXT_2STARS = "";
	public static string COMPLETE_TEXT_3STARS = "";
	public static string COMPLETE_TEXT_HIGHSCORE = "";
	public static string COMPLETE_TEXT_CONTINUE = "";
	public static string COMPLETE_TEXT_SKIP = "";
	public static string COMPLETE_TEXT_NEWHIGHSCORE = "";
	
	//static initializer
	static Globals()
	{
		AdjustFontSizes();
	}
	
	private static void AdjustFontSizes()
	{
		RectButtonStyle.fontSize = (int)FontSize_Medium;
		SquareButtonStyle.fontSize = (int)FontSize_BigMed;
		ObjectiveButtonStyle.fontSize = (int)FontSize_Medium;
		ObjectiveCompleteButtonStyle.fontSize = (int)FontSize_Medium;
		DialogStyle.fontSize = (int)FontSize_Medium;
		ScoreTextStyle.fontSize = (int)FontSize_Bigger;
		ScoreTextCompleteStyle.fontSize = (int)FontSize_Huge;
		ScoreTextCompleteSmallStyle.fontSize = (int)FontSize_Medium;
		TitleCompleteStyle.fontSize = (int)FontSize_Bigger;
		FreeqInfoNumberLeftStyle.fontSize = (int)FontSize_Big;
		FreeqInfoNumberRightStyle.fontSize = (int)FontSize_Big;
		SubParticleInfoNumberStyle.fontSize = (int)FontSize_Medium;
		SubParticleInfoNumberHoverStyle.fontSize = (int)FontSize_Medium;
		DialogTextStyle.fontSize = (int)FontSize_QuiteSmall;
		ParticleInfoNameLeftStyle.fontSize = (int)FontSize_Small;
		ParticleInfoNameRightStyle.fontSize = (int)FontSize_Small;
		GameplayInfoMessageStyle.fontSize = (int)FontSize_Big;
	}
	
	/// <summary>
	/// returns a texture according to the given particle type and isAntiBool
	/// </summary>
	public static Texture2D GetParticleTexture(EItemTypes type, bool isAnti)
	{
		switch(type)
		{
		case EItemTypes.UQuark:
			if(isAnti)
				return PARTICLE_TEXTURE_AUQUARK;
			else
				return PARTICLE_TEXTURE_UQUARK;
			
		case EItemTypes.DQuark:	
			if(isAnti)
				return PARTICLE_TEXTURE_ADQUARK;
			else
				return PARTICLE_TEXTURE_DQUARK;
			
		case EItemTypes.Electron:	
			if(isAnti)
				return PARTICLE_TEXTURE_AELECTRON;
			else
				return PARTICLE_TEXTURE_ELECTRON;
		
		case EItemTypes.Proton:	
			if(isAnti)
				return PARTICLE_TEXTURE_APROTON;
			else
				return PARTICLE_TEXTURE_PROTON;
			
		case EItemTypes.Neutron:	
			if(isAnti)
				return PARTICLE_TEXTURE_ANEUTRON;
			else
				return PARTICLE_TEXTURE_NEUTRON;
		
		case EItemTypes.HeliumNucleus:	
			if(isAnti)
				return PARTICLE_TEXTURE_AHELIUM_NUCLEUS;
			else
				return PARTICLE_TEXTURE_HELIUM_NUCLEUS;
		
		case EItemTypes.CarbonNucleus:	
			if(isAnti)
				return PARTICLE_TEXTURE_ACARBON_NUCLEUS;
			else
				return PARTICLE_TEXTURE_CARBON_NUCLEUS;
		
		case EItemTypes.HeliumAtom:	
			if(isAnti)
				return PARTICLE_TEXTURE_AHELIUM_ATOM;
			else
				return PARTICLE_TEXTURE_HELIUM_ATOM;
		
		case EItemTypes.CarbonAtom:	
			if(isAnti)
				return PARTICLE_TEXTURE_ACARBON_ATOM;
			else
				return PARTICLE_TEXTURE_CARBON_ATOM;
			
		default:
			return PARTICLE_TEXTURE_MISSING;
		}
	}
	
	/// <summary>
	/// returns a texture according to the given particle type, which is a mixture between particle and antiparticle
	/// </summary>
	public static Texture2D GetMixedParticleTexture(EItemTypes type)
	{
		switch(type)
		{
		case EItemTypes.UQuark:
			return MIXED_TEXTURE_UQUARK;
		case EItemTypes.DQuark:	
			return MIXED_TEXTURE_DQUARK;
		case EItemTypes.Electron:	
			return MIXED_TEXTURE_ELECTRON;
		case EItemTypes.Proton:	
			return MIXED_TEXTURE_PROTON;
		case EItemTypes.Neutron:	
			return MIXED_TEXTURE_NEUTRON;

		default:
			return PARTICLE_TEXTURE_MISSING;
		}
	}
	
	/// <summary>
	/// returns a text string which explains particles of the given type
	/// </summary>
	public static string GetParticleText(EItemTypes type, bool isAnti)
	{
		switch(type)
		{
		case EItemTypes.UQuark:
			if(isAnti)
				return PARTICLE_TEXT_AUQUARK;
			else
				return PARTICLE_TEXT_UQUARK;
			
		case EItemTypes.DQuark:	
			if(isAnti)
				return PARTICLE_TEXT_ADQUARK;
			else
				return PARTICLE_TEXT_DQUARK;
			
		case EItemTypes.Electron:
			if(isAnti)
				return PARTICLE_TEXT_AELECTRON;
			else
				return PARTICLE_TEXT_ELECTRON;
			
		case EItemTypes.Proton:	
			if(isAnti)
				return PARTICLE_TEXT_APROTON;
			else
				return PARTICLE_TEXT_PROTON;
			
		case EItemTypes.Neutron:	
			if(isAnti)
				return PARTICLE_TEXT_ANEUTRON;
			else
				return PARTICLE_TEXT_NEUTRON;
			
		case EItemTypes.HeliumNucleus:	
			if(isAnti)
				return PARTICLE_TEXT_AHELIUMNUCLEUS;
			else
				return PARTICLE_TEXT_HELIUMNUCLEUS;
			
		case EItemTypes.HeliumAtom:	
			if(isAnti)
				return PARTICLE_TEXT_AHELIUMATOM;
			else
				return PARTICLE_TEXT_HELIUMATOM;
			
		case EItemTypes.CarbonNucleus:	
			if(isAnti)
				return PARTICLE_TEXT_ACARBONNUCLEUS;
			else
				return PARTICLE_TEXT_CARBONNUCLEUS;
			
		case EItemTypes.CarbonAtom:	
			if(isAnti)
				return PARTICLE_TEXT_ACARBONATOM;
			else
				return PARTICLE_TEXT_CARBONATOM;
			
		default:
			return "Unknown Particle Type";
		}
	}
	
	/// <summary>
	/// returns a text string which represents the name of the given particle type
	/// </summary>
	public static string GetParticleTitle(EItemTypes type, bool isAnti)
	{

		switch(type)
		{
		case EItemTypes.UQuark:
			if(isAnti)
				return PARTICLE_TITLE_AUQUARK;
			else
				return PARTICLE_TITLE_UQUARK;
			
		case EItemTypes.DQuark:	
			if(isAnti)
				return PARTICLE_TITLE_ADQUARK;
			else
				return PARTICLE_TITLE_DQUARK;
			
		case EItemTypes.Electron:
			if(isAnti)
				return PARTICLE_TITLE_AELECTRON;
			else
				return PARTICLE_TITLE_ELECTRON;
			
		case EItemTypes.Proton:	
			if(isAnti)
				return PARTICLE_TITLE_APROTON;
			else
				return PARTICLE_TITLE_PROTON;
			
		case EItemTypes.Neutron:	
			if(isAnti)
				return PARTICLE_TITLE_ANEUTRON;
			else
				return PARTICLE_TITLE_NEUTRON;
			
		case EItemTypes.HeliumNucleus:	
			if(isAnti)
				return PARTICLE_TITLE_AHELIUMNUCLEUS;
			else
				return PARTICLE_TITLE_HELIUMNUCLEUS;
			
		case EItemTypes.HeliumAtom:	
			if(isAnti)
				return PARTICLE_TITLE_AHELIUMATOM;
			else
				return PARTICLE_TITLE_HELIUMATOM;
			
		case EItemTypes.CarbonNucleus:	
			if(isAnti)
				return PARTICLE_TITLE_ACARBONNUCLEUS;
			else
				return PARTICLE_TITLE_CARBONNUCLEUS;
			
		case EItemTypes.CarbonAtom:	
			if(isAnti)
				return PARTICLE_TITLE_ACARBONATOM;
			else
				return PARTICLE_TITLE_CARBONATOM;
			
		default:
			return "Unknown Particle Type";
		}
	}
	
	/// <summary>
	/// Pauses the game.
	/// </summary>
	public static void PauseGame()
	{
		//hide freeqdraught when pausing
		HideOrShowFreeqDraught(false);
		
		AudioManager.Instance.audio.mute = true;
		
		Globals.IsPause = true;
		Time.timeScale = 0;
	}
	
	/// <summary>
	/// Unpauses the game.
	/// </summary>
	public static void UnPauseGame()
	{
		//show freeqdraugh again
		HideOrShowFreeqDraught(true);
		
		AudioManager.Instance.audio.mute = false;
		
		Globals.IsPause = false;
		Time.timeScale = 1;
	}
	
	public static void HideOrShowFreeqDraught(bool show)
	{
		GameObject freeqDraught = GameObject.Find("FreeqDraught");
		if(null != freeqDraught)
			freeqDraught.gameObject.renderer.enabled = show;
	}
}
