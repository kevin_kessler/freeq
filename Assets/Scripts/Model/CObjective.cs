﻿using UnityEngine;
using System.Collections;

public class CObjective 
{
	/**member vars**/
	private EItemTypes mType; //specifies which itemtype needs to be collected
	private bool mIsAnti; //specifies if anti-particle or not
	private int mAmount; //the number of items that have to be collected
	private bool mIsNew; //indicates if the required particle has already been introduced or not
	private bool mIsFulfilled; //wheter the objective is fulfilled or not
	
	/**getters and setters**/
	public EItemTypes Type
	{
		get{ return mType; }
		private set { mType = value; }
	}
	
	public bool IsAnti
	{
		get{ return mIsAnti; }
		private set { mIsAnti = value; }
	}
	
	public int Amount
	{
		get{ return mAmount; }
		private set { mAmount = value; }
	}
	
	public bool IsNew
	{
		get{ return mIsNew; }
		private set { mIsNew = value; }
	}
	
	public bool IsFulfilled
	{
		get{ return mIsFulfilled; }
		set { mIsFulfilled = value; }
	}
	
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="SObjective"/> class.
	/// </summary>
	public CObjective(EItemTypes type, bool isAnti, int amount, bool isNew)
	{
		Type = type;
		IsAnti = isAnti;
		Amount = amount;
		IsNew = isNew;
		IsFulfilled = false;
	}
}
