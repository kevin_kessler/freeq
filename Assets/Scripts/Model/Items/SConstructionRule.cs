﻿using UnityEngine;
using System.Collections;

public struct SConstructionRule 
{
	/**member vars**/
	private EItemTypes mType; //specifies which itemtype is needed for creation
	private int mAmount; //the number of items of this type, that are needed for the creation
	
	/**getters and setters**/
	public EItemTypes Type
	{
		get{ return mType; }
		private set { mType = value; }
	}
	
	public int Amount
	{
		get{ return mAmount; }
		private set { mAmount = value; }
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="SConstructionRule"/> struct.
	/// </summary>
	public SConstructionRule(EItemTypes type, int amount)
	{
		Type = type;
		Amount = amount;
	}
}
