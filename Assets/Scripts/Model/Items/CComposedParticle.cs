﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel; //for readonlycollection

public class CComposedParticle : CItem 
{
	//static members
	private static Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>> mConstructionRulesByType;
	
	//static getters and setters
	public static Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>> ConstructionRulesByType
	{
		get{return mConstructionRulesByType;}
		private set{ mConstructionRulesByType = value; }
	} 
	
	//static initializer
	static CComposedParticle()
	{
		//init the dictionary
		mConstructionRulesByType = new Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>>();
		mConstructionRulesByType.Add(EItemTypes.Proton, ProtonRules());
		mConstructionRulesByType.Add(EItemTypes.Neutron, NeutronRules());
		mConstructionRulesByType.Add(EItemTypes.HeliumNucleus, HeliumNucleusRules());
		mConstructionRulesByType.Add(EItemTypes.HeliumAtom, HeliumAtomRules());
		mConstructionRulesByType.Add(EItemTypes.CarbonNucleus, CarbonNucleusRules());
		mConstructionRulesByType.Add(EItemTypes.CarbonAtom, CarbonAtomRules());
	}
	
	/*****constructionrules****/
	
	/// <summary>
	/// Creates the construction rules for a proton
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> ProtonRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.UQuark, 2));
		rules.Add(new SConstructionRule(EItemTypes.DQuark, 1));
		return rules.AsReadOnly();
	}
	
	/// <summary>
	/// Creates the construction rules for a neutron
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> NeutronRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.UQuark, 1));
		rules.Add(new SConstructionRule(EItemTypes.DQuark, 2));
		return rules.AsReadOnly();
	}
	
	/// <summary>
	/// Creates the construction rules for a helium nucleus
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> HeliumNucleusRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.Proton, 2));
		rules.Add(new SConstructionRule(EItemTypes.Neutron, 2));
		return rules.AsReadOnly();
	}
	
	/// <summary>
	/// Creates the construction rules for a helium atom
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> HeliumAtomRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.HeliumNucleus, 1));
		rules.Add(new SConstructionRule(EItemTypes.Electron, 2));
		return rules.AsReadOnly();
	}
	
	/// <summary>
	/// Creates the construction rules for a carbon nucleus
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> CarbonNucleusRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.Proton, 6));
		rules.Add(new SConstructionRule(EItemTypes.Neutron, 6));
		return rules.AsReadOnly();
	}
	
	/// <summary>
	/// Creates the construction rules for a carbon nucleus
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> CarbonAtomRules()
	{
		List<SConstructionRule> rules = new List<SConstructionRule>();
		rules.Add(new SConstructionRule(EItemTypes.CarbonNucleus, 1));
		rules.Add(new SConstructionRule(EItemTypes.Electron, 6));
		return rules.AsReadOnly();
	}
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CComposedParticle"/> class.
	/// </summary>
	public CComposedParticle(EItemTypes type, bool isAnti): base(type, isAnti)
	{

	}
	
	
	
	
	
}
