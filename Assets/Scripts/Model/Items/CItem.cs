﻿using UnityEngine;
using System.Collections;

public abstract class CItem 
{
	/**static vars**/
	private static int idCounter = 0;
	
	/**member vars**/
	private int mID; //The unique identifier of this item
	private bool mIsAnti; //specifies if this is an anti-particle or a normal one
	private EItemTypes mType; //specifies the type of this item
	
	/**getters & setters**/
	public int ID
	{
		get{ return mID; }
		private set { mID = value; }
	}
	
	public EItemTypes Type
	{
		get{ return mType; }
		private set { mType = value; }
	}
	
	public bool IsAnti
	{
		get{ return mIsAnti; }
		private set { mIsAnti = value; }
	}
	
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CItem"/> class.
	/// </summary>
	protected CItem(EItemTypes type, bool isAnti)
	{
		ID = idCounter++;
		Type = type;
		IsAnti = isAnti;
	}
}
