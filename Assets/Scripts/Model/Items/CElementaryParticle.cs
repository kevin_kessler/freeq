﻿using UnityEngine;
using System.Collections;

public class CElementaryParticle : CItem
{
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CElementary"/> class.
	/// </summary>
	public CElementaryParticle(EItemTypes type, bool isAnti): base(type, isAnti)
	{

	}
}
