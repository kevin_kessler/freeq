﻿using UnityEngine;
using System.Collections;

public enum EItemTypes : int
{
	UQuark = 0,
	DQuark = 1,
	Electron = 2,
	Proton = 3,
	Neutron = 4,
	HeliumNucleus = 5,
	HeliumAtom = 6,
	CarbonNucleus = 7,
	CarbonAtom = 8
}
