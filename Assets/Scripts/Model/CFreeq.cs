﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public class CFreeq 
{
	
	public const int MAX_ITEM_COUNT = 30;

	/**member vars**/
	private List<CItem> mItems;
	private Dictionary<EItemTypes, int> mItemsPerType; //used to track the number of items of the freeq clustered by type
	private Dictionary<EItemTypes, int> mItemsPerAntiType; //TODO: add to UML
	private bool mIsOpened; //indicates wheter the freeq is opened (inventory and items are displayed) or not
	
	/**getters and setters**/
	public List<CItem> Items
	{
		get{ return mItems; }
		private set { mItems = value; }
	}
	
	public Dictionary<EItemTypes, int> ItemsPerType
	{
		get{ return mItemsPerType; }
		private set { mItemsPerType = value; }
	}
	
	public Dictionary<EItemTypes, int> ItemsPerAntiType
	{
		get{ return mItemsPerAntiType; }
		private set { mItemsPerAntiType = value; }
	}
	
	public bool IsOpened
	{
		get{ return mIsOpened; }
		set { mIsOpened = value; }
	}
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CFreeq"/> class.
	/// </summary>
	public CFreeq()
	{
		Items = new List<CItem>();
		InitItemsPerTypeDicts();
	}
	
	
	/**methods**/
	
	/// <summary>
	/// Clears the items.
	/// </summary>
	private void ClearItems()
	{
		Items.Clear();
		ClearItemsPerTypeDicts();
	}
	
	/// <summary>
	/// Adds the given item to the Freeqs' item list.
	/// </summary>
	public void AddItem(CItem item)
	{
		if(!IsFull()){
			Items.Add(item);
			
			if(item.IsAnti)
				ItemsPerAntiType[item.Type] += 1;
			else
				ItemsPerType[item.Type] += 1;
		}
	}
	
	/// <summary>
	/// Removes the given item from the Freeqs' item list.
	/// </summary>
	public void RemoveItem(CItem item)
	{
		if(Items.Contains(item)){
			Items.Remove(item);
			
			if(item.IsAnti)
				ItemsPerAntiType[item.Type] -= 1;
			else
				ItemsPerType[item.Type] -= 1;
		}
	}
	
	/// <summary>
	/// Removes the given items from the Freeq's item list
	/// </summary>
	public void RemoveItems(List<CItem> items)
	{
		foreach(CItem item in items){
			RemoveItem(item);
		}
	}
	
	/// <summary>
	/// Inits the items per type dict.
	/// </summary>
	private void InitItemsPerTypeDicts()
	{
		ItemsPerType = new Dictionary<EItemTypes, int>();
		ItemsPerAntiType = new Dictionary<EItemTypes, int>();
		foreach(EItemTypes type in System.Enum.GetValues(typeof(EItemTypes)))
		{
			ItemsPerType.Add(type,0);
			ItemsPerAntiType.Add(type,0);
		}
	}
	
	/// <summary>
	/// Clears the items per type dicts.
	/// </summary>
	private void ClearItemsPerTypeDicts()
	{
		foreach(EItemTypes type in System.Enum.GetValues(typeof(EItemTypes)))
		{
			ItemsPerType[type] = 0;
			ItemsPerAntiType[type] = 0;
		}
	}
	
	/// <summary>
	/// Determines whether the freeq is full or not.
	/// </summary>
	/// <returns>
	public bool IsFull(){
		return MAX_ITEM_COUNT <= Items.Count;
	}
	
	/// <summary>
	/// Reset the freeqs properties.
	/// </summary>
	public void Reset(){
		ClearItems();
		IsOpened = false;
	}
	
	/// <summary>
	/// Tries to combine the given list of particles to a new particle.
	/// Returns the combined item if successful and null if not.
	/// </summary>
	public CItem CombineParticles(List<CItem> particles)
	{
		CItem combinedItem = null;
		
		//shrink the amount of rules to check, by checking if the number of the given particles fits any existing particle constructionrule
		Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>> rulesToCheck = GetRulesToCheck(particles.Count);
		
		
		if(rulesToCheck.Count > 0)
		{
			//cluster the given particles by type and is antibool
			Dictionary<EItemTypes, Dictionary<bool, int>> numOfParticlesByType = GetNumOfParticlesByType(particles);
			
			//for each rule, check if the given list of particles matches the construction criteria
			foreach(KeyValuePair<EItemTypes, ReadOnlyCollection<SConstructionRule>> pair in rulesToCheck)
			{
				bool canBeConstructed = true;
				bool isAnti = false;
				
				//check normals
				foreach(SConstructionRule rule in pair.Value)
				{
					if(numOfParticlesByType[rule.Type][false] != rule.Amount){
						canBeConstructed = false;
						break;
					}
				}
				
				//check antis
				if(!canBeConstructed)
				{
					isAnti = true;
					canBeConstructed = true;
					foreach(SConstructionRule rule in pair.Value)
					{
						if(numOfParticlesByType[rule.Type][true] != rule.Amount){
							canBeConstructed = false;
							break;
						}
					}
				}

				//create the new particle, add it to the freeq and remove the particles that served for construction
				if(canBeConstructed)
				{
					combinedItem = new CComposedParticle(pair.Key, isAnti);
					RemoveItems(particles);
					AddItem(combinedItem);
					break;
				}
			}
		}
		
		return combinedItem;
	}
	
	/// <summary>
	/// Checks if the given particleAmount matches the needed amount of the ConstructionRules by type.
	/// Returns a list of all constructionrules by type, which match the amount.
	/// </summary>
	private Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>> GetRulesToCheck(int particleAmount)
	{
		Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>> rulesToCheck = new Dictionary<EItemTypes, ReadOnlyCollection<SConstructionRule>>();
		
		foreach(KeyValuePair<EItemTypes, ReadOnlyCollection<SConstructionRule>> rulesByType in CComposedParticle.ConstructionRulesByType)
		{
			//get the amount of required particles for this type
			int numOfRequiredParticles = 0;
			foreach(SConstructionRule rule in rulesByType.Value)
			{
				numOfRequiredParticles += rule.Amount;
			}
			
			//if number equals the given particleAmount, this rule needs to be checked
			if(numOfRequiredParticles == particleAmount)
				rulesToCheck.Add(rulesByType.Key, rulesByType.Value);
		}
		
		return rulesToCheck;
	}
	
	/// <summary>
	/// Examines the given list of particles and creates a Dictionary which clusters them by their type and their isAnti bool.
	/// Returns the created dictionary
	/// </summary>
	private Dictionary<EItemTypes, Dictionary<bool, int>> GetNumOfParticlesByType(List<CItem> particles)
	{
		Dictionary<EItemTypes, Dictionary<bool, int>> numOfParticlesByType = new Dictionary<EItemTypes, Dictionary<bool, int>>();
		foreach(EItemTypes type in System.Enum.GetValues(typeof(EItemTypes)))
		{
			numOfParticlesByType.Add(type, new Dictionary<bool, int>());
			numOfParticlesByType[type].Add(true, 0);
			numOfParticlesByType[type].Add(false, 0);
		}
		
		foreach(CItem particle in particles)
		{
			Dictionary<bool, int> amountByIsAnti = numOfParticlesByType[particle.Type];
			amountByIsAnti[particle.IsAnti] += 1;
		}
		
		return numOfParticlesByType;
	}
}
