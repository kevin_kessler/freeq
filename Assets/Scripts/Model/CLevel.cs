﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CLevel 
{
	/**static vars**/
	private static int idCounter = 0;
	private static int maxNumOfObjectives = 5;
	
	/**member vars**/
	private int mID; //The unique identifier of the level
	private bool mIsLocked; // Specifies if level is locked or accessible
	private int mStartScore; //starting score value when entering the level 
	private int mBottomScore; // min. score that is needed to reach 1 star
	private int mMidScore; // min. score that is needed to reach 2 stars
	private int mTopScore; // min. score that is needed to reach 3 stars
	private int mHighScore; //the max. score ever reached in this level (locally)
	private int mCurrentScore; //the current score while playing the level
	
	private List<CObjective> mObjectives; //the objectives that have to be done to complete this level
	private CFreeq mFreeq; //the container for the collected items
	private bool mSpawnsUQuarks; //wheter upquarks can spawn in this level or not
	private bool mSpawnsDQuarks; //wheter downquarks can spawn in this level or not
	private bool mSpawnsElectrons; //wheter electrons can spawn in this level or not
	
	/**getters & setters**/
	public int ID
	{
		get{ return mID;}
		private set { mID = value; }
	}
	
	public bool IsLocked
	{
		get{ return mIsLocked; }
		set { mIsLocked = value; }
	}
	
	public int StartScore
	{
		get{ return mStartScore; }
		private set { mStartScore = value; }
	}
	
	public int BottomScore
	{
		get{ return mBottomScore; }
		private set { mBottomScore = value; }
	}
	
	public int MidScore
	{
		get{ return mMidScore; }
		private set { mMidScore = value; }
	}
	
	public int TopScore
	{
		get{ return mTopScore; }
		private set { mTopScore = value; }
	}
	
	public int HighScore
	{
		get{ return mHighScore; }
		set { mHighScore = value; }
	}
	
	public int CurrentScore
	{
		get{ return mCurrentScore; }
		set { mCurrentScore = value; }
	}
	
	public List<CObjective> Objectives
	{
		get{ return mObjectives; }
		private set { mObjectives = value; }
	}
	
	public CFreeq Freeq
	{
		get { return mFreeq; }
		private set { mFreeq  = value; }
	}
	
	public bool SpawnsUQuarks
	{
		get{ return mSpawnsUQuarks; }
		set { mSpawnsUQuarks = value; }
	}
	public bool SpawnsDQuarks
	{
		get{ return mSpawnsDQuarks; }
		set { mSpawnsDQuarks = value; }
	}
	public bool SpawnsElectrons
	{
		get{ return mSpawnsElectrons; }
		set { mSpawnsElectrons = value; }
	}
	
	/**constructors**/
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CLevel"/> class.
	/// </summary>
	public CLevel(int startScore, int bottomScore, int midScore, int topScore)
	{
		ID = idCounter++;
		IsLocked = true;
		StartScore = startScore;
		BottomScore = bottomScore;
		MidScore = midScore;
		TopScore = topScore;
		HighScore = 0;
		CurrentScore = startScore;
		
		SpawnsUQuarks = true;
		SpawnsDQuarks = true;
		SpawnsElectrons = true;
		
		Objectives = new List<CObjective>();
		Freeq = new CFreeq();
		
		
	}
	
	/**methods**/
	
	/// <summary>
	/// Creates an objective based on the given parameters and adds it to the levels' objective list.
	/// </summary>
	public void AddObjective(EItemTypes itemType, bool isAnti, int amount, bool isNew)
	{
		if(Objectives.Count < maxNumOfObjectives)
			Objectives.Add(new CObjective(itemType, isAnti, amount, isNew));
		else
			Debug.LogWarning("Max. Number of Objectives reached for the Level with the ID:"+ID);
	}
	
	/// <summary>
	/// Does a soft reset of the level, meaning objectives are resetted and freeq is cleared
	/// </summary>
	public void SoftReset()
	{
		for(int i=0; i<Objectives.Count; i++){
			Objectives[i].IsFulfilled = false;
		}
		
		Freeq.Reset();
		
	}
	
	/// <summary>
	/// Does a hard reset of the level, meaning scores are cleared and level gets locked
	/// </summary>
	public void HardReset()
	{
		SoftReset();
		HighScore = 0;
		IsLocked = true;
	}
}
