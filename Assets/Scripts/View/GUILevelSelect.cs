﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUILevelSelect : MonoBehaviour {
	
	//button rects
	private List<Rect> levelRects;
	private Rect starRect;
	private Rect backRect;
	private Rect helpRect;
	private Rect bgRect;
	
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	private const int levelsPerRow = 4;
	private const float levelRectSize = GUIResize.COMMON_SQUAREBUTTONSIZE;
	private const float marginHor = (screenWidth - levelsPerRow*levelRectSize) / (levelsPerRow+1); //horizontal margin of the level buttons
	private const float marginVer = levelRectSize/2.5f; //vertical margin of the level buttons
	
	//styles
	private Texture2D starTexture;
	private Texture2D lockTexture;
	private Texture2D lockHoverTexture;
	private Texture2D backHoverTexture;
	private Texture2D backTexture;
	private Texture2D helpHoverTexture;
	private Texture2D helpTexture;
	private Texture2D bgTexture;
	private Texture2D overlayTexture;
	
	void Start () 
	{
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//back button on android devices
		if(Input.GetKeyUp(KeyCode.Escape) && !DialogManager.Instance.AnyDialogIsActive)
			LoadMainMenu();
	}
	
	/// <summary>
	/// Inits the levelselect button rects with position and size
	/// </summary>
	private void InitRects()
	{
		//levelRects
		levelRects = new List<Rect>();
		Rect levelRect = new Rect(0, 0, levelRectSize, levelRectSize);
		for(int i = 0; i<LevelManager.Instance.Levels.Count; i++)
		{
			//move row
			if(i % levelsPerRow == 0){
				levelRect.x = marginHor;
				levelRect.y += i==0 ? marginVer : levelRect.height + marginVer;
			}
			
			//add current Rect to levelrect list
			levelRects.Add(levelRect);
			
			//move column
			levelRect.x += levelRect.width + marginHor;
		}
		
		//starRect
		float starSize = levelRect.width /3;
		starRect = new Rect(0,0, starSize, starSize);
		
		//backRect
		backRect = new Rect(GUIResize.CORNER_MARGIN_HOR, screenHeight - GUIResize.COMMON_SQUAREBUTTONSIZE - GUIResize.CORNER_MARGIN_VER, GUIResize.COMMON_SQUAREBUTTONSIZE, GUIResize.COMMON_SQUAREBUTTONSIZE);
		
		//helpRect
		helpRect = new Rect(screenWidth - GUIResize.CORNER_MARGIN_HOR - backRect.width, backRect.yMin, backRect.width, backRect.height);
		
		bgRect = new Rect(0,0,screenWidth, screenHeight);
	}
	
	/// <summary>
	/// Resizes the levelselect button rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		//levelrects
		for(int i=0; i<levelRects.Count; i++)
		{
			levelRects[i] = GUIResize.ResizeRect(levelRects[i]);
		}
		
		//starrect
		starRect = GUIResize.ResizeRect(starRect);
		
		//backrect
		backRect = GUIResize.ResizeRect(backRect);
		
		//helprect
		helpRect = GUIResize.ResizeRect(helpRect);
		
		bgRect = GUIResize.ResizeRect(bgRect);
	}
	
	private void InitStyles()
	{
		starTexture = (Texture2D)Resources.Load("Textures/GUI/star");
		lockTexture = (Texture2D)Resources.Load("Textures/GUI/lock");
		lockHoverTexture = (Texture2D)Resources.Load("Textures/GUI/lock_hovered");
		backTexture = (Texture2D)Resources.Load("Textures/GUI/back");
		backHoverTexture = (Texture2D)Resources.Load("Textures/GUI/back_hovered");
		helpTexture = (Texture2D)Resources.Load("Textures/GUI/help");
		helpHoverTexture = (Texture2D)Resources.Load("Textures/GUI/help_hovered");
		bgTexture = (Texture2D)Resources.Load("Textures/bg");
		overlayTexture = (Texture2D)Resources.Load("Textures/overlay");
	}
	
	//GUI Rendering Function
	void OnGUI()
	{
		//draw bg
		GUI.DrawTexture(bgRect,bgTexture, ScaleMode.ScaleAndCrop);
		GUI.DrawTexture(bgRect,overlayTexture);
		//disable gui if dialog is active
		GUI.enabled = !DialogManager.Instance.AnyDialogIsActive;
		
		//draw levelButtons
		for(int i=0; i<levelRects.Count; i++)
		{
			CLevel curLevel = LevelManager.Instance.Levels[i];
			Rect curLevelRect = levelRects[i];
			bool isLocked = curLevel.IsLocked;
			if(GUI.Button(curLevelRect, isLocked ? "" : (i+1)+"", isLocked ? Globals.SquareButtonImageStyle : Globals.SquareButtonStyle))
			{
				AudioManager.PlayButtonSound();
				if(!isLocked){
					LevelManager.Instance.LoadLevel(curLevel);
				}
			}
			
			
			if(!isLocked)
			{
				//star textures
				if(curLevel.HighScore >= curLevel.BottomScore)
				{
					//1st star
					starRect.x = curLevelRect.xMin;
					starRect.y = curLevelRect.yMax - starRect.height/2;
					GUI.DrawTexture(starRect, starTexture);
					
					if(curLevel.HighScore >= curLevel.MidScore)
					{
						//2nd star
						starRect.x = starRect.xMax;
						GUI.DrawTexture(starRect, starTexture);
						
						if(curLevel.HighScore >= curLevel.TopScore)
						{
							//3rd star
							starRect.x = starRect.xMax;
							GUI.DrawTexture(starRect, starTexture);
						}
					}
				}
			}
			else{
				//lock texture
				bool hovers = curLevelRect.Contains(Event.current.mousePosition);
				GUI.DrawTexture(curLevelRect, hovers ? lockHoverTexture : lockTexture);
			}
		}
		
		//back to main menu button
		bool hovered = backRect.Contains(Event.current.mousePosition);
		if(GUI.Button(backRect , hovered ? backHoverTexture : backTexture, Globals.SquareButtonImageStyle))
		{
			AudioManager.PlayButtonSound();
			LoadMainMenu();
		}
		
		//help button
		hovered = helpRect.Contains(Event.current.mousePosition);
		if(GUI.Button(helpRect, hovered ? helpHoverTexture : helpTexture, Globals.SquareButtonImageStyle))
		{
			AudioManager.PlayButtonSound();
			DialogManager.Instance.ExplainGamePlayDialog.IsActive = true;
		}
	}
	
	/// <summary>
	/// Loads the main menu scene.
	/// </summary>
	private void LoadMainMenu()
	{
		Application.LoadLevel("MainMenu");
	}
}
