﻿using UnityEngine;
using System.Collections;

public abstract class GUIDialog : MonoBehaviour {
	
	private static int idCounter = 0;
	
	//members
	protected int mID;
	protected Rect mDialogRect;
	protected bool mIsActive;
	protected string mTitle;
	protected bool mIsDraggable;
	
	//getters and setters
	public int ID
	{
		get{return mID;}
		protected set{mID = value;}
	}
	
	public Rect DialogRect
	{
		get{return mDialogRect;}
		protected set{mDialogRect = value;}
	}
	
	public bool IsActive
	{
		get{return mIsActive;}
		set{mIsActive = value;}
	}
	
	public string Title
	{
		get{return mTitle;}
		set{mTitle = value;}
	}
	
	public bool IsDraggable
	{
		get{return mIsDraggable;}
		set{mIsDraggable = value;}
	}
	
	//layout
	//protected Rect closeButtonRect;
	public const float closeButtonSize = GUIResize.COMMON_SQUAREBUTTONSIZE;
	protected int titleSize = 35;
	
	//style
	private Texture2D overlayTexture;
	
	//callback
	public delegate void DialogAction();
	public event DialogAction OnCloseClick;
	
	// Use this for initialization
	void Start () 
	{
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyUp(KeyCode.Escape)){
			OnCloseClick();
		}
	}
	
	private void InitRects()
	{
		//closeButtonRect = new Rect(0,0,closeButtonSize,closeButtonSize);
	}
	
	private void ResizeRects()
	{
		//closeButtonRect = GUIResize.ResizeRect(closeButtonRect);
	}
	
	private void InitStyles()
	{
		
	}
	
	void OnGUI()
	{
		if(IsActive){
			
           	

			GUI.depth = - 1; //draw dialog in front of everything else
			
			DialogRect = GUI.Window(ID, DialogRect, DrawDialog, Title, Globals.DialogStyle);
			
			//close button
			/*
			closeButtonRect.x = DialogRect.xMax - closeButtonRect.width/2;
			closeButtonRect.y = DialogRect.yMin - closeButtonRect.height/2;
			if(GUI.Button(closeButtonRect, "Close", Globals.SquareButtonStyle))
			{
				if(null != OnCloseClick)
					OnCloseClick();
			}
			*/
			
			
		}
	}
	
	public void Init(Rect dialogRect)
	{
		ID = idCounter++;
		DialogRect = dialogRect;
		Title = "Dialog Title";
		IsActive = false;
		IsDraggable = false;
		InitContent();
	}
	
	protected abstract void InitContent();
	protected abstract void DrawDialog(int dialogID);
}
