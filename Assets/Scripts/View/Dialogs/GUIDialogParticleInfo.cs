﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public class GUIDialogParticleInfo : GUIDialogInfo 
{

	//members
	private bool mIsAnti;
	private EItemTypes mParticleType;
	private ReadOnlyCollection<SConstructionRule> mConstructionRules;
	private List<EItemTypes> mNavigationHistory;
	
	//getters & setters
	public bool IsAnti
	{
		get{return mIsAnti;}
		set{mIsAnti = value;}
	}
	
	public EItemTypes ParticleType
	{
		get{return mParticleType;}
		set
		{
			mParticleType = value;
			ImageTexture = Globals.GetParticleTexture(mParticleType, IsAnti);
			Text = Globals.GetParticleText(mParticleType, IsAnti);
			Title = Globals.GetParticleTitle(mParticleType, IsAnti);
			ConstructionRules = GetConstructionRules(mParticleType);
			textScrollPos = Vector2.zero;
		}
	}
	
	public ReadOnlyCollection<SConstructionRule> ConstructionRules
	{
		get{return mConstructionRules;}
		private set{mConstructionRules = value;}
	}
	
	//rects
	private Rect subParticleRect;
	private Rect backRect;
	private Rect normalParticleTextRect;
	private Rect antiParticleTextRect;
	
	//styles
	private Texture2D backTexture;
	private Texture2D backHoverTexture;
	
	protected override void InitContent()
	{
		base.InitContent();
		
		InitRects();
		InitStyles();
		
		mNavigationHistory = new List<EItemTypes>();
		OnConfirmClick += ClearNavigationHistory;
		OnCloseClick += ClearNavigationHistory;
	}
	
	private void InitRects()
	{
		//subparticle rect
		subParticleRect = new Rect(textOuterRect.xMin, confirmRect.yMin, confirmRect.height, confirmRect.height);
		backRect = new Rect(confirmRect.xMin - marginHor - confirmRect.height, confirmRect.yMin, confirmRect.height, confirmRect.height);
		
		//textrects for "particle" "anti particle"
		normalParticleTextRect = new Rect(textOuterRect.xMin, imageRect.yMin, imageRect.width/2.5f, imageRect.height/2.5f);
		antiParticleTextRect = new Rect(textOuterRect.xMax - normalParticleTextRect.width, imageRect.yMax - normalParticleTextRect.height, normalParticleTextRect.width, normalParticleTextRect.width);
	}
	
	
	
	private void InitStyles()
	{
		backTexture = (Texture2D)Resources.Load("Textures/GUI/back");
		backHoverTexture = (Texture2D)Resources.Load("Textures/GUI/back_hovered");
	}
	
	protected override void DrawDialog(int dialogID)
	{
		base.DrawDialog(dialogID);
		
		//draw particle name text
		GUI.Box(normalParticleTextRect, Globals.PARTICLE_TEXT, Globals.ParticleInfoNameLeftStyle);
		GUI.Box(antiParticleTextRect, Globals.APARTICLE_TEXT, Globals.ParticleInfoNameRightStyle);
		
		//subparticles
		Rect subRect = subParticleRect;
		foreach(SConstructionRule cr in ConstructionRules)
		{
			//draw subparticle buttons
			if(GUI.Button(subRect, Globals.GetParticleTexture(cr.Type, IsAnti), Globals.SquareButtonImageStyle))
			{
				mNavigationHistory.Add(ParticleType); //add current to navigation history, to be able to go back
				
				DialogManager.Instance.ShowParticleDialog(cr.Type, IsAnti, UnPauseOnConfirm); //show particle info of selected sub particle
			}

			bool hovered = subRect.Contains(Event.current.mousePosition);
			GUI.Box(subRect, cr.Amount+"", hovered ? Globals.SubParticleInfoNumberHoverStyle : Globals.SubParticleInfoNumberStyle);
			
			subRect.x += marginHor + subRect.width;
		}
		
		//draw back to prev. particle button
		if(mNavigationHistory.Count > 0){
			
			bool hovers = backRect.Contains(Event.current.mousePosition);
			if(GUI.Button(backRect, hovers ? backHoverTexture : backTexture,Globals.SquareButtonImageStyle)){
				
				//go to prev. particle info
				DialogManager.Instance.ShowParticleDialog(mNavigationHistory[mNavigationHistory.Count-1], IsAnti, UnPauseOnConfirm);
				mNavigationHistory.RemoveAt(mNavigationHistory.Count-1);
			}
			
		}
	}
	
	/// <summary>
	/// returns a list containing the construction rules for the given type.
	/// returns an empty list if type is an elementary particle
	/// </summary>
	private static ReadOnlyCollection<SConstructionRule> GetConstructionRules(EItemTypes type)
	{
		if(type == EItemTypes.UQuark || type == EItemTypes.DQuark || type == EItemTypes.Electron)
			return new List<SConstructionRule>().AsReadOnly();
		
		return CComposedParticle.ConstructionRulesByType[type];
	}
	
	private void ClearNavigationHistory()
	{
		mNavigationHistory.Clear();
	}
}
