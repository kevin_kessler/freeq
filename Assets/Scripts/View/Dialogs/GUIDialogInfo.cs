﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public class GUIDialogInfo : GUIDialog 
{

	//members
	protected Texture2D mImageTexture;
	protected string mText;
	
	public Texture2D ImageTexture
	{
		get{return mImageTexture;}
		set{mImageTexture = value;}
	}
	
	public string Text
	{
		get{return mText;}
		set{mText = value;}
	}
	
	//callback
	public delegate void DialogAction();
	public event DialogAction OnConfirmClick;
	public bool UnPauseOnConfirm = false;
	
	//rects
	protected Rect confirmRect;
	protected Rect textOuterRect;
	protected Rect textInnerRect;
	protected Rect imageRect;
	
	//layout
	protected float marginVer;
	protected float marginHor;
	
	//text scroll view
	protected Vector2 textScrollPos = Vector2.zero;
	protected bool mouseDragging = false;
	
	protected override void InitContent()
	{
		InitRects();
		InitStyles();
		
		OnConfirmClick += AudioManager.PlayButtonSound;
		OnCloseClick += AudioManager.PlayButtonSound;
	}
	
	private void InitRects()
	{
		//confirm button
		float buttonWidth = DialogRect.width * 0.3f;
		float buttonHeight = buttonWidth / 2f;
		marginHor = DialogRect.width * 0.05f;
		marginVer = marginHor*0.7f;
		
		confirmRect = new Rect(DialogRect.width - buttonWidth - marginHor, DialogRect.height - marginVer - buttonHeight, buttonWidth, buttonHeight);
		
		//image container
		float imageSize = DialogRect.width/1.5f;
		imageRect = new Rect(DialogRect.width/2 - imageSize/2, marginVer + titleSize, imageSize, imageSize);
		
		//message container
		float textWidth = DialogRect.width - 2*marginHor;
		textOuterRect = new Rect(marginHor, imageRect.yMax + marginVer, textWidth, DialogRect.height - imageRect.yMax - 3*marginVer - buttonHeight);
		textInnerRect = new Rect(marginHor+10, textOuterRect.yMin+10, textOuterRect.width-20, textOuterRect.height-20);
	}
	
	
	
	private void InitStyles()
	{
	}
	
	protected override void DrawDialog(int dialogID)
	{
		//image
		GUI.DrawTexture(imageRect, ImageTexture);
		
		//text scrollview
		DrawTextScrollView();
		
		//confirm button
		if(GUI.Button(confirmRect, Globals.STRING_CONFIRM_BUTTON, Globals.RectButtonStyle))
		{
			if(null != OnConfirmClick){
				OnConfirmClick();
				
				if(UnPauseOnConfirm){
					Globals.UnPauseGame();
				}
				
			}
		}
		
		if(IsDraggable)
			GUI.DragWindow(); //makes the dialog window dragable
	}
	
	private void DrawTextScrollView()
	{
		//text scroll view
		GUISkin tempSkin = GUI.skin;
		GUI.skin = Globals.FreeqSkin;
		
		GUI.Box(textOuterRect, "", Globals.DialogTextBackStyle); //frame
		
		HandleTouchScroll();
		
		GUI.BeginGroup(textInnerRect);
		
		textScrollPos = GUILayout.BeginScrollView(textScrollPos, GUILayout.Width(textInnerRect.width), GUILayout.Height(textInnerRect.height));
		GUILayout.Label(Text,Globals.DialogTextStyle); //text
		GUILayout.EndScrollView();
		
		GUI.EndGroup();
		
		GUI.skin = tempSkin;
	}
	
	private void HandleTouchScroll()
	{
		if((textInnerRect.Contains(Event.current.mousePosition) || mouseDragging) && Event.current.type == EventType.MouseDrag) 
		{
		   mouseDragging = true;
		   textScrollPos.y += Event.current.delta.y;
		}
		
		if(mouseDragging && Event.current.type == EventType.MouseUp) 
			mouseDragging = false;
	}
}
