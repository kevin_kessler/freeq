﻿using UnityEngine;
using System.Collections;

public class GUIDialogConfirmation : GUIDialog 
{
	//members
	private string mConfirmationMessage;
	
	//getters & setters
	public string ConfirmationMessage
	{
		get{return mConfirmationMessage;}
		set{mConfirmationMessage = value;}
	}
	
	//callback
	public delegate void DialogAction();
	public event DialogAction OnConfirmClick;
	public event DialogAction OnCancelClick;
	
	//rects
	private Rect confirmRect;
	private Rect cancelRect;
	private Rect messageOuterRect;
	private Rect messageInnerRect;
	
	protected override void InitContent()
	{
		InitRects();
		InitStyles();
		
		OnConfirmClick += AudioManager.PlayButtonSound;
		OnCancelClick += AudioManager.PlayButtonSound;
		OnCloseClick += AudioManager.PlayButtonSound;
	}
	
	private void InitRects()
	{
		float buttonWidth = DialogRect.width * 0.3f;
		float buttonHeight = buttonWidth / 2f;
		float marginVer = DialogRect.width * 0.05f;
		float marginHor = marginVer;
		
		cancelRect = new Rect(marginHor, DialogRect.height - marginVer - buttonHeight, buttonWidth, buttonHeight);
		confirmRect = new Rect(DialogRect.width - buttonWidth - marginHor, cancelRect.yMin, buttonWidth, buttonHeight);
		
		messageOuterRect = new Rect(marginVer, marginHor + titleSize, DialogRect.width - 2*marginHor, DialogRect.height - 3*marginVer - buttonHeight - titleSize);
		messageInnerRect = new Rect(marginHor+10, messageOuterRect.yMin+10, messageOuterRect.width-20, messageOuterRect.height-20);
	}
	
	private void InitStyles()
	{
		
	}
	
	protected override void DrawDialog(int dialogID)
	{
		GUI.Box(messageOuterRect, "", Globals.DialogTextBackStyle); //frame
		GUI.Box(messageInnerRect, ConfirmationMessage, Globals.DialogTextStyle); //message
		
		//no button
		if(GUI.Button(cancelRect, Globals.STRING_NO_BUTTON, Globals.RectButtonStyle))
		{
			if(null != OnCancelClick)
				OnCancelClick();
		}
		
		//yes button
		if(GUI.Button(confirmRect, Globals.STRING_YES_BUTTON, Globals.RectButtonStyle))
		{
			if(null != OnConfirmClick)
				OnConfirmClick();
		}
		
		if(IsDraggable)
			GUI.DragWindow(); //makes the dialog window dragable
	}
}
