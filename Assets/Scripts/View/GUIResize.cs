﻿using UnityEngine;
using System.Collections;

public class GUIResize 
{
	//Nexus 4 Size 768 x 1280
	public const float ORIG_SCREENWIDTH = 768f;
	public const float ORIG_SCREENHEIGHT = 1280f;
	
	//gui consts
	public const float COMMON_SQUAREBUTTONSIZE = 125f; //used for consistent button sizes
	public const float COMMON_RECTBUTTON_HEIGHT = 125f;
	public const float COMMON_RECTBUTTON_WIDTH = 413f;
	public const float CORNER_MARGIN_VER = 50; //used for consistent alignment of corner buttons
	public const float CORNER_MARGIN_HOR = 50; //used for consistent alignment of corner buttons
	
	/// <summary>
	/// Resizes the given rect according to the devices resolution.
	/// </summary>
	public static Rect ResizeRect(Rect rect)
	{
	    float widthFraction = rect.width / ORIG_SCREENWIDTH;
		float heightFraction = rect.height / ORIG_SCREENHEIGHT;
		
	    float width = widthFraction * Screen.width;
	    float height = heightFraction * Screen.height;
		
	    float x = (rect.x / ORIG_SCREENWIDTH) * Screen.width;
	    float y = (rect.y / ORIG_SCREENHEIGHT) * Screen.height;
	 
	    return new Rect(x,y,width,height);
	}
	
}
