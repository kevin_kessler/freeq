﻿using UnityEngine;
using System.Collections;

public class GUIMainMenu : MonoBehaviour {
	
	//button rects
	private Rect playRect;
	private Rect settingsRect;
	private Rect quitRect;
	private Rect bgRect;
	
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	private const float rectButtonWidth = GUIResize.COMMON_RECTBUTTON_WIDTH;
	private const float rectButtonHeight = GUIResize.COMMON_RECTBUTTON_HEIGHT;
	
	//styles
	private Texture2D playTexture;
	private Texture2D playHoverTexture;
	private Texture2D settingsTexture;
	private Texture2D settingsHoverTexture;
	private Texture2D quitTexture;
	private Texture2D quitHoverTexture;
	private Texture2D bgTexture;
	
	//audio
	public static GameObject MusicObject;
	
	void Awake()
	{
		SaveGameManager.Instance.Instantiate();
		AudioManager.SelfInit();
	}
	
	//Called on Initialsation of the script
	void Start () 
	{	
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	//Called once per frame
	void Update () 
	{
		//back button on android devices
		if(Input.GetKeyUp(KeyCode.Escape) && !DialogManager.Instance.AnyDialogIsActive)
			DialogManager.Instance.QuitGameDialog.IsActive = true;
	}
	
	/// <summary>
	/// Inits the main menu button rects with position and size
	/// </summary>
	private void InitRects()
	{
		//play button rect
		playRect = new Rect(screenWidth/2 - rectButtonWidth/2 , screenHeight/2 - rectButtonWidth/2, rectButtonWidth, rectButtonWidth);
		
		//settings and quit rect
		float quitSize = GUIResize.COMMON_SQUAREBUTTONSIZE;
		quitRect = new Rect(GUIResize.CORNER_MARGIN_HOR, screenHeight - quitSize - GUIResize.CORNER_MARGIN_VER, quitSize, quitSize);
		settingsRect = new Rect(screenWidth - GUIResize.CORNER_MARGIN_HOR - quitSize, quitRect.yMin, quitSize, quitSize);
		
		bgRect = new Rect(0,0,screenWidth, screenHeight);
	}
	
	/// <summary>
	/// Resizes the menu button rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		//resize rects to fit for current resolution
		playRect = GUIResize.ResizeRect(playRect);
		settingsRect = GUIResize.ResizeRect(settingsRect);
		quitRect = GUIResize.ResizeRect(quitRect);
		
		bgRect = GUIResize.ResizeRect(bgRect);
	}
	
	private void InitStyles()
	{
		playTexture = (Texture2D)Resources.Load("Textures/GUI/play");
		playHoverTexture = (Texture2D)Resources.Load("Textures/GUI/play_hovered");
		settingsTexture = (Texture2D)Resources.Load("Textures/GUI/settings_gear");
		settingsHoverTexture = (Texture2D)Resources.Load("Textures/GUI/settings_gear_hovered");
		quitTexture = (Texture2D)Resources.Load("Textures/GUI/quit");
		quitHoverTexture = (Texture2D)Resources.Load("Textures/GUI/quit_hovered");
		bgTexture = (Texture2D)Resources.Load("Textures/bg");
	}
	
	//GUI Rendering Function
	void OnGUI()
	{
		//draw bg
		GUI.DrawTexture(bgRect,bgTexture, ScaleMode.ScaleAndCrop);
		
		//disable gui if dialog is active
		GUI.enabled = !DialogManager.Instance.AnyDialogIsActive;
		
		bool hovers = playRect.Contains(Event.current.mousePosition);
		if(GUI.Button( playRect , hovers ? playHoverTexture : playTexture, Globals.SquareButtonImageStyle))
		{
			AudioManager.PlayButtonSound();
			Application.LoadLevel("LevelSelect");
		}
		
		hovers = settingsRect.Contains(Event.current.mousePosition);
		if(GUI.Button( settingsRect , hovers ? settingsHoverTexture : settingsTexture, Globals.SquareButtonImageStyle))
		{
			AudioManager.PlayButtonSound();
			Application.LoadLevel("SettingsMenu");
		}
		
		hovers = quitRect.Contains(Event.current.mousePosition);
		if(GUI.Button( quitRect , hovers ? quitHoverTexture : quitTexture, Globals.SquareButtonImageStyle) )
		{
			AudioManager.PlayButtonSound();
			DialogManager.Instance.QuitGameDialog.IsActive = true;
		}
	}
	
	
}
