﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIGamePlayTopBar 
{
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	public static float panelBottomEdge = 0;
	
	//top panel
	private Rect topPanelRect; //objective & pause container
	private const float scoreHeight = 60;
	private const float objectiveNumbersHeight = 40;
	public const float topPanelHeight = GUIResize.COMMON_SQUAREBUTTONSIZE + scoreHeight + objectiveNumbersHeight;
	private Rect scoreRect;
	private Rect pauseRect; //pause button rect
	private const float pauseRectSize = GUIResize.COMMON_SQUAREBUTTONSIZE;
	private Rect objectivesRect; //the objective container
	private List<Rect> objectiveRects; //objective button rects
	private static float objectiveRectWidth = GUIResize.COMMON_SQUAREBUTTONSIZE;
	private static float objectiveRectHeight = objectiveRectWidth + objectiveNumbersHeight;
	private List<Rect> objectiveImageRects;
	
	//styles
	private Texture2D playTexture;
	private Texture2D playHoverTexture;
	private Texture2D pauseTexture;
	private Texture2D pauseHoverTexture;
	
	public GUIGamePlayTopBar()
	{
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	/// <summary>
	/// Inits the top panel rects with position and size
	/// </summary>
	private void InitRects()
	{
		topPanelRect = new Rect(0,0,screenWidth,topPanelHeight);
		pauseRect = new Rect(topPanelRect.xMax - pauseRectSize, topPanelRect.yMin, pauseRectSize, pauseRectSize);
		objectivesRect = new Rect(topPanelRect.xMin,topPanelRect.yMin, topPanelRect.width - pauseRectSize, topPanelRect.height - scoreHeight); 
		scoreRect = new Rect(topPanelRect.xMin, objectivesRect.yMax, topPanelRect.width, scoreHeight);
		
		//single objective rects
		objectiveRects = new List<Rect>();
		objectiveImageRects = new List<Rect>();
		Rect objectiveRect = new Rect(0, 0, objectiveRectWidth, objectiveRectHeight);
		float objectiveImagePadding = 10;
		Rect objectiveImageRect = new Rect(objectiveImagePadding, objectiveImagePadding, objectiveRectWidth-2*objectiveImagePadding, objectiveRectWidth-2*objectiveImagePadding);
		for(int i=0; i<LevelManager.Instance.CurrentLevel.Objectives.Count; i++)
		{
			if(i != 0){
				objectiveRect.x += objectiveRectWidth;
				objectiveImageRect.x += objectiveRectWidth;
			}
			
			objectiveRects.Add(objectiveRect);
			objectiveImageRects.Add(objectiveImageRect);
		}
	}
	
	/// <summary>
	/// Resizes the top panel rects according to current resolution
	/// </summary>
	private void ResizeRects()
	{
		topPanelRect = GUIResize.ResizeRect(topPanelRect);
		panelBottomEdge = topPanelRect.yMax;
		
		pauseRect = GUIResize.ResizeRect(pauseRect);
		objectivesRect = GUIResize.ResizeRect(objectivesRect);
		
		for(int i = 0; i<objectiveRects.Count; i++){
			objectiveRects[i] = GUIResize.ResizeRect(objectiveRects[i]);
			objectiveImageRects[i] = GUIResize.ResizeRect(objectiveImageRects[i]);
		}

		scoreRect = GUIResize.ResizeRect(scoreRect);
	}
	
	private void InitStyles()
	{	
		playTexture = (Texture2D)Resources.Load("Textures/GUI/play");
		playHoverTexture = (Texture2D)Resources.Load("Textures/GUI/play_hovered");
		pauseTexture = (Texture2D)Resources.Load("Textures/GUI/pause");
		pauseHoverTexture = (Texture2D)Resources.Load("Textures/GUI/pause_hovered");
	}
	
	/// <summary>
	/// Draws the top panel.
	/// </summary>
	public void Draw()
	{
		if(null != LevelManager.Instance.CurrentLevel)
		{
			//Objectives
			DrawObjectives();
			
			//pause button
			bool hovers = pauseRect.Contains(Event.current.mousePosition);
			if(GUI.Button(pauseRect, Globals.IsPause ? (hovers ? playHoverTexture : playTexture) : (hovers ? pauseHoverTexture : pauseTexture), Globals.SquareButtonImageStyle))
			{
				AudioManager.PlayButtonSound();
				if(Globals.IsPause)
					Globals.UnPauseGame();
				else
					Globals.PauseGame();
			}
			
			//score bar
			GUI.Box(scoreRect, LevelManager.Instance.CurrentLevel.CurrentScore.ToString("D5"), Globals.ScoreTextStyle); //D5 sets up to 5 leading zeros in front of the number
		}
	}
	
	/// <summary>
	/// Draws the objectives part of the top panel
	/// </summary>
	private void DrawObjectives()
	{
		//Objectives
		GUI.BeginGroup(objectivesRect); //begin objectives group
		
		for(int i=0; i<objectiveRects.Count; i++)
		{
			CObjective objective = LevelManager.Instance.CurrentLevel.Objectives[i];
			if(null != objective)
			{
				string btnText = "";
				int neededAmount = objective.Amount;
				int haveAmount = 0;
				if(objective.IsAnti){
					haveAmount = LevelManager.Instance.CurrentLevel.Freeq.ItemsPerAntiType[objective.Type];
				}
				else{
					haveAmount = LevelManager.Instance.CurrentLevel.Freeq.ItemsPerType[objective.Type];
				}
				btnText+= haveAmount+"/"+neededAmount;
				
				bool complete = neededAmount <= haveAmount;
				if(GUI.Button(objectiveRects[i], btnText, complete ? Globals.ObjectiveCompleteButtonStyle : Globals.ObjectiveButtonStyle))
				{
					AudioManager.PlayButtonSound();
					DialogManager.Instance.ShowParticleDialog(objective.Type, objective.IsAnti, !Globals.IsPause);
					Globals.PauseGame();
				}
				
				GUI.DrawTexture(objectiveImageRects[i], Globals.GetParticleTexture(objective.Type, objective.IsAnti) );
			}
		}
		GUI.EndGroup(); //end objectives group
	}

}
