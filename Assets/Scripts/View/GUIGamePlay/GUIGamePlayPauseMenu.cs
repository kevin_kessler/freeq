﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIGamePlayPauseMenu 
{
	//layout params
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	private const float rectButtonWidth = GUIResize.COMMON_RECTBUTTON_WIDTH;
	private const float rectButtonHeight = GUIResize.COMMON_RECTBUTTON_HEIGHT;
	
	//rects
	private Rect buttonGroupRect; //pause menu buttons grouprect 
	private Rect restartRect;
	private Rect levelSelectRect;
	private Rect soundRect;
	private Rect helpRect;
	private Rect bgRect;
	private const int numberOfButtonRows = 4;
	
	//style
	private Texture2D overlayTexture;
	private Texture2D bgTexture;
	
	public GUIGamePlayPauseMenu()
	{
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	public void Update()
	{
			
	}
	
	/// <summary>
	/// Inits the pause menu rects with position and size
	/// </summary>
	private void InitRects()
	{
		float buttonGroupHeight = screenHeight/2f;
		buttonGroupRect = new Rect(screenWidth/2f - rectButtonWidth/2, screenHeight/2f - buttonGroupHeight/2, rectButtonWidth, buttonGroupHeight);
		float marginVer = (buttonGroupRect.height - numberOfButtonRows*rectButtonHeight) / (numberOfButtonRows-1);
		
		//restart rect
		restartRect = new Rect(0, 0, buttonGroupRect.width, rectButtonHeight);
		
		//levelSelect rect
		levelSelectRect = new Rect(0, restartRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		//sound rect
		soundRect = new Rect(0, levelSelectRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		//help rect
		helpRect = new Rect(0, soundRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		bgRect = new Rect(0,0,screenWidth, screenHeight);
	}
	
	/// <summary>
	/// Resizes the pause menu rects according to current resolution
	/// </summary>
	private void ResizeRects()
	{
		buttonGroupRect = GUIResize.ResizeRect(buttonGroupRect);
		
		soundRect = GUIResize.ResizeRect(soundRect);
		restartRect = GUIResize.ResizeRect(restartRect);
		levelSelectRect = GUIResize.ResizeRect(levelSelectRect);
		helpRect = GUIResize.ResizeRect(helpRect);
		
		bgRect = GUIResize.ResizeRect(bgRect);
	}
	
	private void InitStyles()
	{	
		overlayTexture = (Texture2D)Resources.Load("Textures/overlay");
		bgTexture = (Texture2D)Resources.Load("Textures/bg");
	}
	
	/// <summary>
	/// Draws the pause menu.
	/// </summary>
	public void Draw()
	{
		GUI.enabled = !DialogManager.Instance.AnyDialogIsActive;
		
		GUI.DrawTexture(bgRect, bgTexture, ScaleMode.ScaleAndCrop);
		if(GUI.enabled){
			GUI.DrawTexture(bgRect, overlayTexture);
		}
		
		
		//begin button group
		GUI.BeginGroup(buttonGroupRect);
		
		//restart button
		if(GUI.Button( restartRect , Globals.STRING_RESTART_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			Globals.UnPauseGame();
			LevelManager.Instance.LoadLevel( LevelManager.Instance.CurrentLevel );
		}
		
		//levelSelect button
		if(GUI.Button( levelSelectRect , Globals.STRING_LEVELSELECT_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			LevelManager.Instance.UnsetCurrentLevel();
			Globals.UnPauseGame();
			Application.LoadLevel("LevelSelect");
		}
		
		//sound on/off toggle
		if(GUI.Button(soundRect, Globals.AudioIsOn ? Globals.STRING_AUDIO_BUTTON_ON : Globals.STRING_AUDIO_BUTTON_OFF, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			Globals.AudioIsOn = !Globals.AudioIsOn;
			if(Globals.AudioIsOn){
				AudioListener.volume = 1;
			}
			else{
				AudioListener.volume = 0;
			}
			
			//save settings
			SaveGameManager.Instance.SaveSettings();
		}
		
		//help button
		if(GUI.Button( helpRect , Globals.STRING_HELP_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			DialogManager.Instance.ExplainGamePlayDialog.IsActive = true;
		}
		
		//end button group
		GUI.EndGroup();
		
	}
}
