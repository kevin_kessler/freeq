﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIGamePlay : MonoBehaviour 
{
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	
	//rects
	private Rect fadeMessageRect;
	
	//gui parts
	private GUIGamePlayTopBar topBar;
	private GUIGamePlayFreeqBar freeqBar;
	private GUIGamePlayPauseMenu pauseMenu;
	
	//controlvars
	public bool LevelIsComplete = false;
	private bool completionTriggered = false;
	
	//fade message stuff
	private bool showMessage = false;
	private float messageFadeValue = 0;
	private string messageText = "";
	private GUIStyle messageFontStyle = Globals.GameplayInfoMessageStyle;
	
	//audio
	AudioClip CompleteAudio;
	bool CompleteAudioPlayed = false;
	
	void Awake()
	{
		AudioManager.Instance.PlayGamePlayMusic();
	}
	
	// Use this for initialization
	void Start () 
	{
		//if there is no current level, show level select instead
		if(null == LevelManager.Instance.CurrentLevel){
			Application.LoadLevel("LevelSelect");
			return;
		}
		
		//Create GUI Parts
		topBar = new GUIGamePlayTopBar();
		freeqBar = new GUIGamePlayFreeqBar(this);
		pauseMenu = new GUIGamePlayPauseMenu();
		
		//init rects
		InitRects();
		ResizeRects();
		InitAudio();
	}
	
	/// <summary>
	/// Inits the the rects with position and size
	/// </summary>
	private void InitRects()
	{
		float messageRectHeight = screenHeight/6;
		float messageRectWidth = screenWidth/1.3f;
		fadeMessageRect = new Rect(screenWidth/2 - messageRectWidth/2, GUIGamePlayTopBar.panelBottomEdge + 53, messageRectWidth, messageRectHeight);
	}
	
	/// <summary>
	/// Resizes the rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		fadeMessageRect = GUIResize.ResizeRect(fadeMessageRect);
	}
	
	private void InitAudio()
	{
		CompleteAudio = (AudioClip)Resources.Load("Audio/level_complete");
	}
	
	// Update is called once per frame
	void Update () 
	{
		//back button on android devices
		if(Input.GetKeyUp(KeyCode.Escape)){
			if(Globals.IsPause){
				if(!DialogManager.Instance.AnyDialogIsActive)
					Globals.UnPauseGame();
			}
			else
				Globals.PauseGame();
		}
		
		if(null != freeqBar && !Globals.IsPause)
			freeqBar.Update();
	}
	
	
	
	/// <summary>
	/// Renders the GameplayGUI
	/// </summary>
	void OnGUI()
	{
		if(null != LevelManager.Instance.CurrentLevel)
		{
			//draw gameplay gui
			if(!CompleteAudioPlayed)
			{
				if(!Globals.IsPause)
				{
					if(null != freeqBar)
						freeqBar.Draw();
					
				}
				else{
					if(null != pauseMenu)
						pauseMenu.Draw();
				}
				
				if(null != topBar)
					topBar.Draw();
				
				if(showMessage)
					DrawMessage();
			}
			
			//start completion gui
			if(LevelIsComplete && !completionTriggered)
			{
				StartCoroutine(CompleteLevel());
				completionTriggered = true;
			}
			
		}
	}
	
	private void DrawMessage()
	{
		//backup gui color
		Color backUpColor = GUI.color;
		
		//fade gui color
		Color tmpColor = GUI.color;
		tmpColor.a = messageFadeValue;
		GUI.color = tmpColor;

		//draw message
		GUI.Box(fadeMessageRect, messageText, messageFontStyle);
		
		//set gui color back to original
		GUI.color = backUpColor;
	}
	
	public IEnumerator CompleteLevel()
	{
		AudioManager.Instance.audio.Stop(); //stop music
		AudioSource.PlayClipAtPoint(CompleteAudio, Camera.main.transform.position); //play complete sound
		
		yield return new WaitForSeconds(CompleteAudio.length);
		CompleteAudioPlayed = true;
		
		gameObject.AddComponent("GUIGamePlayLevelComplete");
	}
	
	public void StartFadeMessageRoutine(float fadeInDuration, float fadeOutDuration, float stayDuration, string message, Color fontColor)
	{
		StartCoroutine( ShowFadeMessage(fadeInDuration, fadeOutDuration, stayDuration, message, fontColor) );
	}
	
	private IEnumerator ShowFadeMessage(float fadeInDuration, float fadeOutDuration, float stayDuration, string message, Color fontColor)
	{
		showMessage = true;
		
		messageText = message;
		messageFontStyle.normal.textColor = fontColor;
		
		//fade in
		for (float i = 0.0f; i <= 1.0f; i += Time.deltaTime*(1/fadeInDuration)) 
		{ 
			messageFadeValue = Mathf.Lerp(0f, 1.1f, i); 
		    yield return null;
	   	}
		
		//stay for reading
		for (float i = 0.0f; i <= 1.0f; i += Time.deltaTime*(1/stayDuration)) 
		{ 
			yield return null;
		}
		
		//fade out
		for (float i = 0.0f; i <= 1.0f; i += Time.deltaTime*(1/fadeOutDuration)) 
		{ 
			messageFadeValue = Mathf.Lerp(1.1f, 0f, i); 
		    yield return null;
	   	}
		
		showMessage = false;
	}
	
	void OnDestroy()
	{
		if(null != AudioManager.Instance)
			AudioManager.Instance.PlayMenuMusic();
	}
}
