﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GUIFreeqStates : int
{
	Minimized = 0,
	Maximizing = 1,
	Maximized = 2,
	Minimizing = 3
}

public class GUIGamePlayFreeqBar
{
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	public float maximizeLimit = GUIGamePlayTopBar.panelBottomEdge; //where to stop maximizing
	public GUIFreeqStates freeqState;
	public float maximizeSpeed = Screen.height * 0.028f;
	
	//freeq panel
	private static Rect freeqRect; //container for freeqTube, upArrows and inventory info
	private Rect infosRect; //the inventory info container
	private Rect headRect; //the rect that contains upArrows and freeqTube
	private const float infoNumbersHeight = 40;
	private const float infosRectHeight = GUIResize.COMMON_SQUAREBUTTONSIZE + infoNumbersHeight;
	private const float headRectHeight = 75;
	public static float freeqRectHeight = infosRectHeight+headRectHeight;
	
	private Rect freeqArrowLeftRect;
	private Rect freeqArrowRightRect;
	public static Rect freeqTubeRect;
	
	private Rect infoRectUQuark; //upQuark Inforect
	private Rect infoRectDQuark; //downQuark Inforect
	private Rect infoRectElectron; //electron Inforect
	private Rect infoRectProton; //proton Inforect
	private Rect infoRectNeutron; //neutron Inforect
	private const int numOfInfoRects = 5;
	private float infoRectWidth;
	private Rect infoImageRect; //image rect inside of the info rects
	
	public static Rect dropZone; //the rect where the particles have to be released, to be moved to the freeq
	
	private GUIGamePlayFreeqInventory inventory;
	
	//styles
	private Texture2D freeqbarHeadTexture;
	private Texture2D freeqbarHeadDownTexture;
	private Texture2D freeqbarHeadRedTexture;
	private Texture2D freeqbarHeadDownRedTexture;
	private Texture2D freeqbarBodyTexture;
	private Texture2D freeqbarBodyRedTexture;
	
	//audio
	private AudioClip DoorSound;
	
	private GUIGamePlay gamePlayComponent;
	
	public GUIGamePlayFreeqBar(GUIGamePlay _gamePlayComponent)
	{
		freeqState = GUIFreeqStates.Minimized;
		
		gamePlayComponent = _gamePlayComponent;
		inventory = new GUIGamePlayFreeqInventory(gamePlayComponent);
		
		InitRects();
		ResizeRects();
		InitStyles();
		InitAudio();
	}
	
	/// <summary>
	/// Inits the freeq panel rects with position and size
	/// </summary>
	private void InitRects()
	{
		freeqRect = new Rect(0,screenHeight - freeqRectHeight, screenWidth, freeqRectHeight);
		headRect = new Rect(0, 0, freeqRect.width, headRectHeight);
		infosRect = new Rect(0, headRect.height, freeqRect.width, infosRectHeight);
		
		
		//head rect content
		freeqArrowLeftRect = new Rect(0,0, headRect.width*1/4, headRectHeight);
		freeqArrowRightRect = new Rect(headRect.width - freeqArrowLeftRect.width, 0, freeqArrowLeftRect.width, headRectHeight);
		float tubeRectWidth = headRect.width/2;
		freeqTubeRect = new Rect(headRect.width/2 - tubeRectWidth/2, 0, tubeRectWidth,  headRectHeight);
		
		dropZone = freeqTubeRect;
		float dropZoneYOffset = freeqTubeRect.height;
		dropZone.height = freeqRectHeight + 2*dropZoneYOffset;
		dropZone.y = freeqRect.y - dropZoneYOffset;
		
		
		//single info rects
		infoRectWidth = infosRect.width / numOfInfoRects;
		Rect infoRect = new Rect(0, 0, infoRectWidth, infosRect.height);
		float imagePaddingVer = infoRect.height /7;
		float imagePaddingHor = infoRect.width /7;
		infoImageRect = new Rect(imagePaddingHor, imagePaddingVer, infoRect.width - 2*imagePaddingHor, infoRect.height - 2*imagePaddingVer);
		
		infoRectUQuark = infoRect;
		
		infoRect.x += infoRectWidth;
		infoRectDQuark = infoRect;
		
		infoRect.x += infoRectWidth;
		infoRectElectron = infoRect;
		
		infoRect.x += infoRectWidth;
		infoRectProton = infoRect;
		
		infoRect.x += infoRectWidth;
		infoRectNeutron = infoRect;
	}
	
	/// <summary>
	/// Resizes the freeq panel rects according to current resolution
	/// </summary>
	private void ResizeRects()
	{
		freeqRect = GUIResize.ResizeRect(freeqRect);
		
		infosRect = GUIResize.ResizeRect(infosRect);
		headRect = GUIResize.ResizeRect(headRect);
		
		freeqArrowLeftRect = GUIResize.ResizeRect(freeqArrowLeftRect);
		freeqArrowRightRect = GUIResize.ResizeRect(freeqArrowRightRect);
		freeqTubeRect = GUIResize.ResizeRect(freeqTubeRect);
		
		dropZone = GUIResize.ResizeRect(dropZone);
		
		infoRectUQuark = GUIResize.ResizeRect(infoRectUQuark);
		infoRectDQuark = GUIResize.ResizeRect(infoRectDQuark);
		infoRectElectron = GUIResize.ResizeRect(infoRectElectron);
		infoRectProton = GUIResize.ResizeRect(infoRectProton);
		infoRectNeutron = GUIResize.ResizeRect(infoRectNeutron);
		
		infoImageRect = GUIResize.ResizeRect(infoImageRect);
	}
	
	private void InitStyles()
	{	
		freeqbarHeadTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_head");
		freeqbarHeadDownTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_head_down");
		freeqbarHeadRedTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_head_red");
		freeqbarHeadDownRedTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_head_down_red");
		freeqbarBodyTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_body");
		freeqbarBodyRedTexture = (Texture2D)Resources.Load("Textures/GUI/freeqbar_body_red");
	}
	
	private void InitAudio()
	{
		DoorSound = (AudioClip)Resources.Load("Audio/door");
	}
	
	public void Update()
	{
		//check if freeq panel needs to be moved
		CheckFreeqState();
	}
	
	/// <summary>
	/// Draws the freeq panel.
	/// </summary>
	public void Draw()
	{
		GUI.enabled = !Globals.IsPause; //disable freeq interaction when game is paused
		
		bool freeqFull = LevelManager.Instance.CurrentLevel.Freeq.IsFull();
		if(freeqFull)
			Globals.HideOrShowFreeqDraught(false);
		else
			Globals.HideOrShowFreeqDraught(true);
		
		//GUI.Box(dropZone, freeqFull ? "Freeq is FULL!" : "DROPHERE");
		
		GUI.BeginGroup(freeqRect); //freeq group begin (head area and info area)
		
		//headrect content
		GUI.BeginGroup(headRect); //headgroup begin (up Arrows and freeqTube)
		/*
		if(GUI.Button(freeqArrowLeftRect, GUIContent.none, GUIStyle.none) || GUI.Button(freeqArrowRightRect, GUIContent.none, GUIStyle.none))
		{
			StartMaximizeOrMinimize();
		}
		*/
		//GUI.Box(freeqTubeRect, freeqFull ? "Tube Closed" : "Tube");
		GUI.EndGroup(); //headgroup end
		
		bool showUpArrows = freeqState == GUIFreeqStates.Minimized || freeqState == GUIFreeqStates.Minimizing;
		Texture2D headTextureToShow = showUpArrows ? (freeqFull ? freeqbarHeadRedTexture : freeqbarHeadTexture) : (freeqFull ? freeqbarHeadDownRedTexture : freeqbarHeadDownTexture);
		GUI.DrawTexture(headRect, headTextureToShow);
		GUI.DrawTexture(infosRect, freeqFull ? freeqbarBodyRedTexture : freeqbarBodyTexture);
		
		//info rects
		
		//info group begin (info rects)
		GUI.BeginGroup(infosRect);
		
		DrawParticleInfoRect(infoRectUQuark, EItemTypes.UQuark);
		DrawParticleInfoRect(infoRectDQuark, EItemTypes.DQuark);
		DrawParticleInfoRect(infoRectElectron, EItemTypes.Electron);
		DrawParticleInfoRect(infoRectProton, EItemTypes.Proton);
		DrawParticleInfoRect(infoRectNeutron, EItemTypes.Neutron);
		
		GUI.EndGroup(); //info group end
		
		GUI.EndGroup(); //freeq group end
		
		//maximize or minimize on freeq click
		if(GUI.Button(freeqRect, GUIContent.none, GUIStyle.none))
		{
			StartMaximizeOrMinimize();
		}
		
		//draw inventory
		if(freeqState != GUIFreeqStates.Minimized)
		{
			if(!LevelManager.Instance.CurrentLevel.Freeq.IsOpened)
				LevelManager.Instance.CurrentLevel.Freeq.IsOpened = true;
				
			inventory.Draw();
			
		}else{
			if(LevelManager.Instance.CurrentLevel.Freeq.IsOpened)
				LevelManager.Instance.CurrentLevel.Freeq.IsOpened = false;
		}
	}
	
	/// <summary>
	/// Draws the respective particle info rect of the freeq bar
	/// </summary>
	private void DrawParticleInfoRect(Rect infoRect, EItemTypes particleType)
	{
		Dictionary<EItemTypes, int> itemsPerType = LevelManager.Instance.CurrentLevel.Freeq.ItemsPerType;
		Dictionary<EItemTypes, int> itemsPerAntiType = LevelManager.Instance.CurrentLevel.Freeq.ItemsPerAntiType;
		
		//GUI.Box(infoRect, "", Globals.DialogTextStyle);
		GUI.BeginGroup(infoRect);
		GUI.DrawTexture(infoImageRect, Globals.GetMixedParticleTexture(particleType));
		GUI.EndGroup();
		
		GUI.Box(infoRect, itemsPerType[particleType]+"", Globals.FreeqInfoNumberLeftStyle);
		GUI.Box(infoRect, itemsPerAntiType[particleType]+"",  Globals.FreeqInfoNumberRightStyle);
	}
	
	/// <summary>
	/// Sets the freeqState to Maximizing or Minimizing, depending on the current state.
	/// </summary>
	private void StartMaximizeOrMinimize()
	{
		if(freeqState == GUIFreeqStates.Minimized || freeqState == GUIFreeqStates.Minimizing){
			freeqState = GUIFreeqStates.Maximizing;
			Globals.HideOrShowFreeqDraught(false);
		}
		else if(freeqState == GUIFreeqStates.Maximized || freeqState == GUIFreeqStates.Maximizing){
			freeqState = GUIFreeqStates.Minimizing;
			Globals.HideOrShowFreeqDraught(true);
		}
		
		AudioSource.PlayClipAtPoint(DoorSound, Camera.main.transform.position);
	}
	
	/// <summary>
	/// Checks the gui state of the freeq and handles minimizing and maximizing.
	/// </summary>
	private void CheckFreeqState()
	{
		//move freeq rect up or down, depending on the state
		switch(freeqState)
		{
		case GUIFreeqStates.Minimized:
			break;
		case GUIFreeqStates.Maximizing:
			MaximizeFreeq();
			break;
		case GUIFreeqStates.Maximized:
			break;
		case GUIFreeqStates.Minimizing:
			MinimizeFreeq();
			break;
		}
	}
	
	/// <summary>
	/// Maximizes the freeq, meaning the freeqRect moves to the top
	/// </summary>
	private void MaximizeFreeq()
	{
		if(freeqRect.yMin > maximizeLimit)
		{
			if((freeqRect.y - maximizeSpeed) < maximizeLimit){
				freeqRect.y = maximizeLimit;
				GUIGamePlayFreeqInventory.inventoryRect.y = freeqRect.yMax;
			}
			else{
				freeqRect.y -= maximizeSpeed;
				GUIGamePlayFreeqInventory.inventoryRect.y -= maximizeSpeed;
			}
		}
		else{
			freeqState = GUIFreeqStates.Maximized;
		}
	}
	
	/// <summary>
	/// Minimizes the freeq, meaning the freeqRect moves to the bottom
	/// </summary>
	private void MinimizeFreeq()
	{
		if(freeqRect.yMax < Screen.height){
			if((freeqRect.yMax + maximizeSpeed) > Screen.height){
				freeqRect.y = Screen.height - freeqRect.height;
				GUIGamePlayFreeqInventory.inventoryRect.y = freeqRect.yMax;
			}
			else{
				freeqRect.y += maximizeSpeed;
				GUIGamePlayFreeqInventory.inventoryRect.y += maximizeSpeed;
			}
		}
		else{
			freeqState = GUIFreeqStates.Minimized;
		}
	}
}

