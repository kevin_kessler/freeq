﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIGamePlayFreeqInventory {
	
	private List<CItem> items;
	private List<CItem> selectedItems;
	
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	private const int itemsPerRow = 6;
	
	public static Rect inventoryRect; //the inventory grouprect
	private Rect itemRect;
	private Rect releaseRect; //release button rect
	private Rect combineRect; //combine button rect
	
	//style
	private Texture2D backGroundTexture;
	
	//audio
	private AudioClip ErrorSound;
	private AudioClip CombineSound;
	private AudioClip SelectSound;
	
	private GUIGamePlay gamePlayComponent;
	
	public GUIGamePlayFreeqInventory(GUIGamePlay _gamePlayComponent) 
	{
		InitRects();
		ResizeRects();
		InitStyles();
		InitAudio();
		
		gamePlayComponent = _gamePlayComponent;
		
		items = LevelManager.Instance.CurrentLevel.Freeq.Items;
		selectedItems = new List<CItem>();
	}
	

	public void Update () 
	{

	}
	
	/// <summary>
	/// Inits the inventory rects with position and size
	/// </summary>
	private void InitRects()
	{
		float inventoryHeight = screenHeight - (GUIGamePlayTopBar.topPanelHeight + GUIGamePlayFreeqBar.freeqRectHeight);
		inventoryRect = new Rect(0,screenHeight,screenWidth, inventoryHeight);
		
		//itemRect
		float itemSize = inventoryRect.width / itemsPerRow;
		itemRect = new Rect(0,0,itemSize,itemSize);
		
		//release and combine
		float cornerRectsHeight = itemSize;
		releaseRect = new Rect(0, inventoryRect.height - cornerRectsHeight, itemSize*2, cornerRectsHeight);
		combineRect = new Rect(inventoryRect.width - releaseRect.width, releaseRect.yMin, releaseRect.width, releaseRect.height);
		
	}
	
	/// <summary>
	/// Resizes the levelselect button rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		inventoryRect = GUIResize.ResizeRect(inventoryRect);
		itemRect = GUIResize.ResizeRect(itemRect);
		releaseRect = GUIResize.ResizeRect(releaseRect);
		combineRect = GUIResize.ResizeRect(combineRect);
	}
	
	private void InitStyles()
	{
		backGroundTexture = (Texture2D)Resources.Load("Textures/GUI/grey");
	}
	
	private void InitAudio()
	{
		ErrorSound = (AudioClip)Resources.Load("Audio/error");
	    CombineSound = (AudioClip)Resources.Load("Audio/construct");
		SelectSound = (AudioClip)Resources.Load("Audio/click");
	}
	
	//GUI Rendering Function
	public void Draw()
	{
		GUI.DrawTexture(inventoryRect, backGroundTexture);
		
		//Begin Inventory Group
		GUI.BeginGroup(inventoryRect);
		
		
		//draw items
		Rect auxRect = itemRect;
		for(int i=0; i<CFreeq.MAX_ITEM_COUNT; i++)
		{
			//move row
			if( (i != 0) && (i % itemsPerRow == 0)){
				auxRect.x = 0;
				auxRect.y += auxRect.height;
			}
			
			//item slot
			if(items.Count > i){
				//itemtext
				CItem item = items[i];
				bool selected = selectedItems.Contains(item);
				//string btnText = (item.IsAnti ? "Anti-" : "")+" "+item.Type+" "+(selected ? "\n(sel)" : "");
				
				//item button
				if(GUI.Button(auxRect, Globals.GetParticleTexture(item.Type, item.IsAnti), selected ? Globals.SquareButtonItemSelected : Globals.SquareButtonItem))
				{
					if(selected)
						selectedItems.Remove(item);
					else
						selectedItems.Add(item);
					
					AudioSource.PlayClipAtPoint(SelectSound, Camera.main.transform.position);
				}
			}
			//empty slot
			else{
				//item button
				GUI.Button(auxRect, "", Globals.SquareButtonItem);
			}
			
			//move column
			auxRect.x += auxRect.width;
		}
		
		bool itemsSelected = selectedItems.Count > 0;
		
		//draw combine and release button
		if(GUI.Button(combineRect, Globals.STRING_COMBINE_BUTTON, Globals.RectButtonStyle)){
			CItem combinedItem = LevelManager.Instance.CurrentLevel.Freeq.CombineParticles(selectedItems);
			if(null != combinedItem){
				selectedItems = new List<CItem>();
				string creationMessage = Globals.GetParticleTitle(combinedItem.Type, combinedItem.IsAnti)+" "+Globals.PARTICLE_CREATED_TEXT;
				gamePlayComponent.StartFadeMessageRoutine(0.25f,0.25f,1, creationMessage, Color.green);
				AudioSource.PlayClipAtPoint(CombineSound, Camera.main.transform.position);
			}
			else{
				if(itemsSelected){
					gamePlayComponent.StartFadeMessageRoutine(0.25f,0.25f,1, Globals.PARTICLE_COMBINATIONERROR_TEXT, Color.red);
				}
				else{
					gamePlayComponent.StartFadeMessageRoutine(0.25f,0.25f,1, Globals.PARTICLE_NOTSELECTED_TEXT, Color.red);
				}
				
				AudioSource.PlayClipAtPoint(ErrorSound, Camera.main.transform.position);
			}
		}
		
		if(GUI.Button(releaseRect, Globals.STRING_RELEASE_BUTTON, Globals.RectButtonStyle)){
			
			if(itemsSelected){
				LevelManager.Instance.CurrentLevel.Freeq.RemoveItems(selectedItems);
				selectedItems = new List<CItem>();
				AudioManager.PlayButtonSound();
			}
			else{
				gamePlayComponent.StartFadeMessageRoutine(0.25f,0.25f,1, Globals.PARTICLE_NOTSELECTED_TEXT, Color.red);
				AudioSource.PlayClipAtPoint(ErrorSound, Camera.main.transform.position);
			}
		}
		
		//End Inventory Group
		GUI.EndGroup();

	}
	
	/// <summary>
	/// Loads the main menu scene.
	/// </summary>
	private void LoadMainMenu()
	{
		Application.LoadLevel("MainMenu");
	}
}
