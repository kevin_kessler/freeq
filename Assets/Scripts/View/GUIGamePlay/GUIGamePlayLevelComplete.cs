﻿using UnityEngine;
using System.Collections;

public class GUIGamePlayLevelComplete : MonoBehaviour {
	
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	
	//rects
	private Rect groupBGRect; //rect that contains the group rect
	private Rect groupRect; //rect that contains all other rects
	private Rect titleRect; //rect for the title message
	private Rect starRect1; //rect for the 1st star
	private Rect starRect2; //rect for the 2nd star
	private Rect starRect3; //rect for the 3rd star
	private Rect currentScoreRect; //score counter rect
	private Rect bestScoreRect; //previous high score rect
	private Rect messageRect; //rect for text message
	private Rect confirmRect; //confirmation button rect
	
	//styles
	private Texture2D noStarTexture;
	private Texture2D starTexture;
	
	//contolvars
	private float scoreCounter = 0; //displayed counting score
	private float countTime = 2.5f; //time to count from 0 to reached score in seconds
	private CLevel currentLevel;
	private string scoreText;
	private float guiFadeValue = 0;
	private float guiFadeTextValue = 0;
	private bool guiFading = false;
	private bool isCounting = true;
	
	//audio
	private bool playedSoundStar1 = false;
	private bool playedSoundStar2 = false;
	private bool playedSoundStar3 = false;
	private AudioClip sparkleSound;
	private AudioSource scoreCounterSound;
	
	// Use this for initialization
	void Start () 
	{
		InitRects();
		ResizeRects();
		InitStyles();
		InitAudio();
		
		currentLevel = LevelManager.Instance.CurrentLevel;
		scoreText =  GetScoreText();
		
		StartCoroutine(GUIFade(0,1.1f,0.5f, false));
	}

	/// <summary>
	/// Inits the the level complete gui rects with position and size
	/// </summary>
	private void InitRects()
	{
		float bgMarginHor = 75;
		float bgMarginVer = 75;
		float innerMarginHor = 25;
		float innerMarginVer = 25;
		
		//group container rect
		groupBGRect = new Rect(bgMarginHor, bgMarginVer, screenWidth - 2*bgMarginHor, screenHeight - 2*bgMarginVer);
		groupRect = new Rect(innerMarginHor, innerMarginVer, groupBGRect.width - 2*innerMarginHor, groupBGRect.height - 2*innerMarginVer);
		
		//title rect
		titleRect = new Rect(0, 0, groupRect.width, 150);
		
		//star rects
		float starSize = 128;
		float starMarginVer = 100; //distance between titlerect and star rects
		float starMarginHor = (groupRect.width - 3*starSize)/2;
		starRect1 = new Rect(0,titleRect.yMax + starMarginVer, starSize,starSize);
		starRect2 = new Rect(starRect1.xMax+starMarginHor, starRect1.yMin, starSize, starSize);
		starRect3 = new Rect(starRect2.xMax+starMarginHor, starRect1.yMin, starSize, starSize);
		
		//score rects
		currentScoreRect = new Rect(0,starRect1.yMax + 50, groupRect.width, 75);
		bestScoreRect = new Rect(0, currentScoreRect.yMax + 25, groupRect.width, 50);
		
		//confirm button rect
		float buttonWidth = groupRect.width * 0.33f;
		confirmRect = new Rect(groupRect.width - buttonWidth, groupRect.height - GUIResize.COMMON_SQUAREBUTTONSIZE, buttonWidth, GUIResize.COMMON_SQUAREBUTTONSIZE);
		
		//text message rect
		messageRect = new Rect(0, bestScoreRect.yMax + starMarginVer, groupRect.width, confirmRect.yMin - (bestScoreRect.yMax + starMarginVer) - starMarginVer);
	}
	
	/// <summary>
	/// Resizes the rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		//resize rects to fit for current resolution
		groupBGRect = GUIResize.ResizeRect(groupBGRect);
		groupRect = GUIResize.ResizeRect(groupRect);
		
		titleRect = GUIResize.ResizeRect(titleRect);
		starRect1 = GUIResize.ResizeRect(starRect1);
		starRect2 = GUIResize.ResizeRect(starRect2);
		starRect3 = GUIResize.ResizeRect(starRect3);
		currentScoreRect = GUIResize.ResizeRect(currentScoreRect);
		bestScoreRect = GUIResize.ResizeRect(bestScoreRect);
		messageRect = GUIResize.ResizeRect(messageRect);
		
		confirmRect = GUIResize.ResizeRect(confirmRect);
	}
	
	private void InitStyles()
	{
		noStarTexture = (Texture2D)Resources.Load("Textures/GUI/no_star");
		starTexture = (Texture2D)Resources.Load("Textures/GUI/star");
	}
	
	private void InitAudio()
	{
		sparkleSound = (AudioClip)Resources.Load("Audio/ding");
		
		scoreCounterSound = (AudioSource)gameObject.AddComponent("AudioSource");
		scoreCounterSound.clip = (AudioClip)Resources.Load("Audio/counter");
		scoreCounterSound.loop = true;
		scoreCounterSound.Play();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//count score
		CountScore();
		
		//start fade text if count is done
		if(currentLevel.CurrentScore <= scoreCounter && !guiFading)
		{
			guiFading = true;
			StartCoroutine(GUIFade(0,1.1f,0.5f, true));
		}
	}
	
	/// <summary>
	/// Counts the displayed score (scoreCounter) upwards up to the current levels reached score (currentScore).
	/// </summary>
	private void CountScore()
	{
		if(currentLevel.CurrentScore <= scoreCounter){
			scoreCounter = currentLevel.CurrentScore;
			isCounting = false;
		}
		else{
			scoreCounter += CalcCountStepSize();
		}
		
		if(!isCounting){
			scoreCounterSound.Stop();
		}
	}
	
	/// <summary>
	/// calc step size in the way that the calling loop needs exactly countTime in seconds to reach the reached score
	/// </summary>
	/// <returns>
	/// The count step size.
	/// </returns>
	private float CalcCountStepSize()
	{
		//how many milliseconds passed since the last update call ?
		float intervalCount = (Time.deltaTime * 1000); 
		if(intervalCount < 1)
			intervalCount = 1;
		
		return currentLevel.CurrentScore / countTime / 1000 * intervalCount; 
	}
	
	void OnGUI()
	{
		//fade gui
		Color tmpColor = GUI.color;
		tmpColor.a = guiFadeValue;
		GUI.color = tmpColor;
		
		GUI.Box(groupBGRect, "", Globals.DialogTextBackStyle);
		GUI.BeginGroup(groupBGRect); //container bg rect
		GUI.BeginGroup(groupRect); //container begin
		
		//draw title
		GUI.Box(titleRect, "Level "+ (currentLevel.ID+1)+ " " + Globals.COMPLETE_TEXT_CLEAR +"!", Globals.TitleCompleteStyle );
		
		//Draw stars
		DrawStars();
		
		//draw counting score
		GUI.Box(currentScoreRect, ((int)scoreCounter)+"", Globals.ScoreTextCompleteStyle);
		
		//draw confirm button
		DrawConfirmButton();
		
		//draw score text and use fade value for gui
		tmpColor.a = guiFadeTextValue;
		GUI.color = tmpColor;
		bool beatHighscore = currentLevel.HighScore <= currentLevel.CurrentScore;
		GUI.Box(bestScoreRect, beatHighscore ? Globals.COMPLETE_TEXT_NEWHIGHSCORE : Globals.COMPLETE_TEXT_HIGHSCORE+": "+currentLevel.HighScore, Globals.ScoreTextCompleteSmallStyle);
		GUI.Box(messageRect, scoreText, Globals.ScoreTextCompleteSmallStyle);
		
		
		
		GUI.EndGroup(); //container end
		GUI.EndGroup(); //container bg end
	}
	
	private void DrawStars()
	{
		bool star1Reached = scoreCounter >= currentLevel.BottomScore;
		bool star2Reached = scoreCounter >= currentLevel.MidScore;
		bool star3Reached = scoreCounter >= currentLevel.TopScore;
		
		GUI.DrawTexture(starRect1, star1Reached ? starTexture : noStarTexture);
		GUI.DrawTexture(starRect2, star2Reached ? starTexture : noStarTexture);
		GUI.DrawTexture(starRect3, star3Reached ? starTexture : noStarTexture);
		
		//play sounds on appearance
		if(star1Reached && !playedSoundStar1){
			AudioSource.PlayClipAtPoint(sparkleSound, Camera.main.transform.position);
			playedSoundStar1 = true;
		}
		
		if(star2Reached && !playedSoundStar2){
			AudioSource.PlayClipAtPoint(sparkleSound, Camera.main.transform.position);
			playedSoundStar2 = true;
		}
		
		if(star3Reached && !playedSoundStar3){
			AudioSource.PlayClipAtPoint(sparkleSound, Camera.main.transform.position);
			playedSoundStar3 = true;
		}
	}

	
	private void DrawConfirmButton()
	{
		bool ready = currentLevel.CurrentScore <= scoreCounter;
		//bool hovered = confirmRect.Contains(Event.current.mousePosition);
		if(GUI.Button(confirmRect, ready ? Globals.COMPLETE_TEXT_CONTINUE : Globals.COMPLETE_TEXT_SKIP, Globals.RectButtonStyle)){
			LevelManager.Instance.UnsetCurrentLevel();
			Globals.UnPauseGame();
			AudioManager.PlayButtonSound();
			Application.LoadLevel("LevelSelect");
		}
	}
	
	private string GetScoreText()
	{
		bool star1Reached = currentLevel.CurrentScore >= currentLevel.BottomScore;
		bool star2Reached = currentLevel.CurrentScore >= currentLevel.MidScore;
		bool star3Reached = currentLevel.CurrentScore >= currentLevel.TopScore;
		
		if(star3Reached)
			return Globals.COMPLETE_TEXT_3STARS;
		else if(star2Reached)
			return Globals.COMPLETE_TEXT_2STARS;
		else if(star1Reached)
			return Globals.COMPLETE_TEXT_1STAR;
		else
			return Globals.COMPLETE_TEXT_NOSTAR;
	}
	
	private IEnumerator GUIFade(float startVal, float endVal, float duration, bool fadeTextOnly)
	{
		for (float i = 0.0f; i <= 1.0f; i += Time.deltaTime*(1/duration)) 
		{ 
			float fadeVal = Mathf.Lerp(startVal, endVal, i); 
			if(fadeTextOnly)
				guiFadeTextValue = fadeVal;
			else
				guiFadeValue = fadeVal;
		    yield return null;
	   }
	}
}
