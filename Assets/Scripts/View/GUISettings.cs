﻿using UnityEngine;
using System.Collections;

public class GUISettings : MonoBehaviour {
	
	//layout params (originalScreenWidth = 768f; originalScreenHeight = 1280f;)
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	private const float rectButtonWidth = GUIResize.COMMON_RECTBUTTON_WIDTH;
	private const float rectButtonHeight = GUIResize.COMMON_RECTBUTTON_HEIGHT;
	
	//button rects
	private Rect buttonGroupRect;
	private Rect soundRect;
	private bool soundToggle;
	private Rect languageRect;
	private Rect languageFlagRect;
	private Rect aboutRect;
	private Rect resetRect;
	private Rect helpRect;
	private const int numberOfButtonRows = 5;
	private Rect backRect;
	private Rect bgRect;
	
	
	//styles
	private Texture2D backHoverTexture;
	private Texture2D backTexture;
	private Texture2D bgTexture;
	private Texture2D overlayTexture;
	
	//Called on Initialsation of the script
	void Start () 
	{	
		InitRects();
		ResizeRects();
		InitStyles();
	}
	
	//Called once per frame
	void Update () 
	{
		//back button on android devices
		if(Input.GetKeyUp(KeyCode.Escape) && !DialogManager.Instance.AnyDialogIsActive)
			BackToMain();
	}
	
	/// <summary>
	/// Inits the settings menu button rects with position and size
	/// </summary>
	private void InitRects()
	{
		float buttonGroupHeight = screenHeight/1.5f;
		buttonGroupRect = new Rect(screenWidth/2f - rectButtonWidth/2, screenHeight/2f - buttonGroupHeight/2 - GUIResize.COMMON_SQUAREBUTTONSIZE/2, rectButtonWidth, buttonGroupHeight);
		float marginVer = (buttonGroupRect.height - numberOfButtonRows*rectButtonHeight) / (numberOfButtonRows-1);
		
		//help rect
		helpRect = new Rect(0, 0, buttonGroupRect.width, rectButtonHeight);
		
		//audio on/off button rect
		soundRect = new Rect(0, helpRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		//lang rect
		languageRect = new Rect(0, soundRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		languageFlagRect = new Rect(languageRect.xMax - rectButtonHeight/2 - 20, languageRect.yMin, rectButtonHeight/2, rectButtonHeight/2);
		
		//about rect
		aboutRect = new Rect(0, languageRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		//reset rect
		resetRect = new Rect(0, aboutRect.yMax + marginVer, buttonGroupRect.width, rectButtonHeight);
		
		//settings and quit button rect
		float backSize = GUIResize.COMMON_SQUAREBUTTONSIZE;
		backRect = new Rect(0 + GUIResize.CORNER_MARGIN_HOR, screenHeight - backSize - GUIResize.CORNER_MARGIN_VER, backSize, backSize);
		
		bgRect = new Rect(0,0,screenWidth, screenHeight);
	}
	
	/// <summary>
	/// Resizes the settings menu button rects according to the current resolution
	/// </summary>
	private void ResizeRects()
	{
		//resize rects to fit for current resolution
		buttonGroupRect = GUIResize.ResizeRect(buttonGroupRect);
		
		soundRect = GUIResize.ResizeRect(soundRect);
		languageRect = GUIResize.ResizeRect(languageRect);
		languageFlagRect = GUIResize.ResizeRect(languageFlagRect);
		aboutRect = GUIResize.ResizeRect(aboutRect);
		resetRect = GUIResize.ResizeRect(resetRect);
		helpRect = GUIResize.ResizeRect(helpRect);
		
		backRect = GUIResize.ResizeRect(backRect);
		
		bgRect = GUIResize.ResizeRect(bgRect);
	}
	
	private void InitStyles()
	{
		backTexture = (Texture2D)Resources.Load("Textures/GUI/back");
		backHoverTexture = (Texture2D)Resources.Load("Textures/GUI/back_hovered");
		bgTexture = (Texture2D)Resources.Load("Textures/bg");
		overlayTexture = (Texture2D)Resources.Load("Textures/overlay");
	}
	

	//GUI Rendering Function
	void OnGUI()
	{
		//draw bg
		GUI.DrawTexture(bgRect,bgTexture, ScaleMode.ScaleAndCrop);
		GUI.DrawTexture(bgRect,overlayTexture);
		
		//disable gui if dialog is active
		GUI.enabled = !DialogManager.Instance.AnyDialogIsActive;
		
		//begin button group
		GUI.BeginGroup(buttonGroupRect);
		
		//help button
		if(GUI.Button( helpRect , Globals.STRING_HELP_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			DialogManager.Instance.ExplainGamePlayDialog.IsActive = true;
		}
		
		//sound on/off toggle
		if(GUI.Button(soundRect, Globals.AudioIsOn ? Globals.STRING_AUDIO_BUTTON_ON : Globals.STRING_AUDIO_BUTTON_OFF, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			Globals.AudioIsOn = !Globals.AudioIsOn;
			if(Globals.AudioIsOn){
				AudioListener.volume = 1;
			}
			else{
				AudioListener.volume = 0;
			}
		}
		
		//change language button
		if(GUI.Button( languageRect , Globals.STRING_LANGUAGE_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			LanguageManager.Instance.LoadNextLanguage();
		}
		//lang flag texture
		GUI.DrawTexture(languageFlagRect, LanguageManager.Instance.GetCurrentLanguageIcon());
		
		//about button
		if(GUI.Button( aboutRect , Globals.STRING_ABOUT_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			DialogManager.Instance.AboutGameDialog.IsActive = true;
		}
		
		//reset button
		if(GUI.Button( resetRect , Globals.STRING_RESET_BUTTON, Globals.RectButtonStyle))
		{
			AudioManager.PlayButtonSound();
			
			//show confirmation dialog
			DialogManager.Instance.ResetProgressDialog.IsActive = true;
		}
		
		//end button group
		GUI.EndGroup();
		
		//back button
		bool hovers = backRect.Contains(Event.current.mousePosition);
		if(GUI.Button( backRect , hovers ? backHoverTexture : backTexture, Globals.SquareButtonImageStyle) )
		{
			AudioManager.PlayButtonSound();
			BackToMain();
		}
	}
	
	/// <summary>
	/// Saves settings and goes back to main menu
	/// </summary>
	private void BackToMain()
	{
		SaveGameManager.Instance.SaveSettings();
		Application.LoadLevel("MainMenu");
	}
}
