﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Singleton LevelManager
public class LevelManager : Singleton<LevelManager> 
{
	//members
	private List<CLevel> mLevels; //collection of all levels
	private CLevel mCurrentLevel; //the level currently played
	
	//getters and setters
	public List<CLevel> Levels
	{
		get{ return mLevels; }
		private set { mLevels = value; }
	}
	
	public CLevel CurrentLevel
	{
		get{ return mCurrentLevel; }
		private set { mCurrentLevel = value; }
	}
	
	//control vars
	private bool currentLevelComplete = false;
	
	/// <summary>
	/// Private constructor, to prevent initialisation from outside.
	/// </summary>
	private LevelManager()
	{
		InitLevels();
	}
	
	void Awake(){
		SaveGameManager.Instance.Load(); //TODO: remove on final build
		AudioManager.SelfInit(); //TODO: remove on final build
	}
	
	void Update()
	{
		if(null != CurrentLevel)
		{		
			//complete level if objectives are fulfilled
			if(!currentLevelComplete && ObjectivesAreFulfilled())
			{
				CompleteLevel();
			}
		}
	}
	
	/// <summary>
	/// fulfills the tasks that need to be done when all objectives are fullfilled.
	/// including setting the highscore, unlocking the next level, save the game, and notifying the gameplay gui that level is complete in order to start the completion gui step
	/// </summary>
	private void CompleteLevel()
	{
		SetHighScore();
		UnlockNextLevel();
		SaveGameManager.Instance.Save();
		
		//notify gameplay gui
		bool notified = NotifyGamePlayGUI();
		if(!notified){
			UnsetCurrentLevel();
			Application.LoadLevel("LevelSelect");
		}
		else{
			DestroyGamePlayObjects();
		}
		
		currentLevelComplete = true;
	}
	
	/// <summary>
	/// Notifies the game play GUI that all objectives of the current level are complete.
	/// </summary>
	/// <returns>
	/// Returns a bool that indicates whether the gui has been notified successfully
	/// </returns>
	private bool NotifyGamePlayGUI()
	{
		GameObject gamePlayManager = GameObject.Find("Manager");
		bool notified = false;
		if(null != gamePlayManager){
			GUIGamePlay gamePlayGUI = (GUIGamePlay)gamePlayManager.GetComponent("GUIGamePlay");
			if(null != gamePlayGUI){
				gamePlayGUI.LevelIsComplete = true;
				notified = true;
			}
		}
		return notified;
	}
	
	/// <summary>
	/// Destroys the objects of the gameplay scene which are not wanted during level completion gui step
	/// </summary>
	private void DestroyGamePlayObjects()
	{
		//SpawnManager
		GameObject spawnManager = GameObject.Find("SpawnManager");
		if(null != spawnManager)
			Destroy(spawnManager);
		
		//FreeqDraught
		GameObject freeqDraught = GameObject.Find("FreeqDraught");
		if(null != freeqDraught)
			Destroy(freeqDraught);
		
		//ScoreManager
		GameObject scoreManager = GameObject.Find("ScoreManager");
		if(null != scoreManager)
			Destroy(scoreManager);
	}
	
	/// <summary>
	/// Checks if all objectives of the level are fullfilled or not
	/// </summary>
	private bool ObjectivesAreFulfilled()
	{
		bool fulfilled = true;
		
		foreach(CObjective objective in CurrentLevel.Objectives)
		{
			int neededAmount = objective.Amount;
			int haveAmount = 0;
			
			if(objective.IsAnti){
				haveAmount = CurrentLevel.Freeq.ItemsPerAntiType[objective.Type];
			}
			else{
				haveAmount = CurrentLevel.Freeq.ItemsPerType[objective.Type];
			}
			
			if(haveAmount >= neededAmount)
				objective.IsFulfilled = true;
			else{
				objective.IsFulfilled = false;
				fulfilled = false;
			}
		}
		
		return fulfilled;
	}
	
	
	
	/// <summary>
	/// Resets the objectives of the level and sets CurrentLevel to null
	/// </summary>
	public void UnsetCurrentLevel()
	{
		CurrentLevel.SoftReset();		
		CurrentLevel = null;
		currentLevelComplete = false;
	}
	
	/// <summary>
	/// Unlocks the next level int the list (the one after CurrentLevel)
	/// </summary>
	private void UnlockNextLevel()
	{
		int indexOfCurrentLevel = Levels.IndexOf(CurrentLevel);
		if(indexOfCurrentLevel+1 < Levels.Count){
			Levels[indexOfCurrentLevel+1].IsLocked = false;
			Debug.Log("Level "+ (Levels[indexOfCurrentLevel+1].ID+1) + " unlocked!");
		}
	}
	
	/// <summary>
	/// Sets a new highscore for the current level, if the currentScore is higher.
	/// </summary>
	private void SetHighScore()
	{
		if(CurrentLevel.CurrentScore > CurrentLevel.HighScore)
			CurrentLevel.HighScore = CurrentLevel.CurrentScore;
	}
	
	/// <summary>
	/// Sets the given level as current level and loads gameplay scene
	/// </summary>
	/// <param name='level'>
	/// Level.
	/// </param>
	public void LoadLevel(CLevel level)
	{
		//set current level
		CurrentLevel = level;
		CurrentLevel.SoftReset();
		
		//check if need to introduce particle
		List<CObjective> objectives = level.Objectives;
		foreach(CObjective objective in objectives){
			if(objective.IsNew){
				Globals.PauseGame();
				DialogManager.Instance.ShowParticleDialog(objective.Type, objective.IsAnti, true);
				//TODO: improve that, because particle dialog of the dialogmanager is one instance, meaning only the last called "showParticleDialog" will be shown.
			}	
		}
		
		//load gameplay scene
		Application.LoadLevel("GamePlay");

	}
	
	/// <summary>
	/// does a hard reset for all levels, meaning scores are cleared and levels get locked
	/// </summary>
	public void HardResetAllLevels()
	{
		//reset all levels
		foreach(CLevel level in Levels)
		{
			level.HardReset();
		}
		
		//unlock first level
		if(Levels.Count > 0)
			Levels[0].IsLocked = false;
	}
	
	/// <summary>
	/// Inits the levels and adds them to the level list.
	/// </summary>
	private void InitLevels()
	{
		//NOTE: Currently only one new particle can be introduced per level. 
		//introducing means: show introduction dialog about the particle at the beginning of the level. (see LoadLevel(CLevel level) function)
		
		Levels = new List<CLevel>();
		Levels.Add( Level01() );
		Levels.Add( Level02() );
		Levels.Add( Level03() );
		Levels.Add( Level04() );
		Levels.Add( Level05() );
		Levels.Add( Level06() );
		Levels.Add( Level07() );
		Levels.Add( Level08() );
		Levels.Add( Level09() );
		Levels.Add( Level10() );
		Levels.Add( Level11() );
		Levels.Add( Level12() );
		Levels.Add( Level13() );
		Levels.Add( Level14() );
		Levels.Add( Level15() );
		Levels.Add( Level16() );
		Levels.Add( Level17() );
		Levels.Add( Level18() );
		Levels.Add( Level19() );
		Levels.Add( Level20() );
		Levels.Add( Level21() );
		Levels.Add( Level22() );
		Levels.Add( Level23() );
		Levels.Add( Level24() );
	}
	
	/// <summary>
	/// Creates Level01 and returns it.
	/// </summary>
	private CLevel Level01()
	{
		CLevel level = new CLevel(5000, 1750, 2750, 4000); //ok
		level.IsLocked = false;
		level.AddObjective(EItemTypes.UQuark, false, 5, true); //isAnti, amount, isNew
		level.SpawnsDQuarks = false;
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level02 and returns it.
	/// </summary>
	private CLevel Level02()
	{
		CLevel level = new CLevel(7000, 2000, 4000, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.UQuark, false, 10, false); //isAnti, amount, isNew
		level.SpawnsDQuarks = false;
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level03 and returns it.
	/// </summary>
	private CLevel Level03()
	{
		CLevel level = new CLevel(7000, 2000, 4000, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.UQuark, true, 10, true); //isAnti, amount, isNew
		level.SpawnsDQuarks = false;
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level04 and returns it.
	/// </summary>
	private CLevel Level04()
	{
		CLevel level = new CLevel(7000, 2500, 4500, 5500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.UQuark, false, 6, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.UQuark, true, 6, false);
		level.SpawnsDQuarks = false;
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level05 and returns it.
	/// </summary>
	private CLevel Level05()
	{
		CLevel level = new CLevel(7000, 2000, 4000, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.DQuark, false, 10, true); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		level.SpawnsUQuarks = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level06 and returns it.
	/// </summary>
	private CLevel Level06()
	{
		CLevel level = new CLevel(7000, 2500, 4500, 5500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.DQuark, false, 6, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.DQuark, true, 6, true); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		level.SpawnsUQuarks = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level07 and returns it.
	/// </summary>
	private CLevel Level07()
	{
		CLevel level = new CLevel(8000, 2000, 4000, 5000);
		level.IsLocked = true;
		level.AddObjective(EItemTypes.UQuark, false, 5, false);
		level.AddObjective(EItemTypes.UQuark, true, 5, false);
		level.AddObjective(EItemTypes.DQuark, false, 5, false);
		level.AddObjective(EItemTypes.DQuark, true, 5, false); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level08 and returns it.
	/// </summary>
	private CLevel Level08()
	{
		CLevel level = new CLevel(9000, 2000, 3500, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Proton, false, 3, true); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level09 and returns it.
	/// </summary>
	private CLevel Level09()
	{
		CLevel level = new CLevel(10000, 3000, 4500, 6000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Proton, false, 3, false);
		level.AddObjective(EItemTypes.Proton, true, 3, true); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level10 and returns it.
	/// </summary>
	private CLevel Level10()
	{
		CLevel level = new CLevel(9000, 2000, 3500, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Neutron, false, 3, true); //isAnti, amount, isNew
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level11 and returns it.
	/// </summary>
	private CLevel Level11()
	{
		CLevel level = new CLevel(10000, 3000, 4500, 6000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Neutron, false, 3, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Neutron, true, 3, true);
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level12 and returns it.
	/// </summary>
	private CLevel Level12()
	{
		CLevel level = new CLevel(12000, 3500, 5000, 7000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Proton, false, 2, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Proton, true, 2, false);
		level.AddObjective(EItemTypes.Neutron, false, 2, false);
		level.AddObjective(EItemTypes.Neutron, true, 2, false);
		level.SpawnsElectrons = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level13 and returns it.
	/// </summary>
	private CLevel Level13()
	{
		CLevel level = new CLevel(7000, 2000, 4000, 5000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Electron, false, 10, true); //isAnti, amount, isNew
		level.SpawnsDQuarks = false;
		level.SpawnsUQuarks = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level14 and returns it.
	/// </summary>
	private CLevel Level14()
	{
		CLevel level = new CLevel(7000, 2500, 4500, 5500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.Electron, false, 6, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, true, 6, true); //isAnti, amount, isNew
		level.SpawnsDQuarks = false;
		level.SpawnsUQuarks = false;
		
		return level;
	}
	
	/// <summary>
	/// Creates Level15 and returns it.
	/// </summary>
	private CLevel Level15()
	{
		CLevel level = new CLevel(10000, 2000, 4000, 6000); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.UQuark, false, 6, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.DQuark, false, 6, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, false, 3, false); //isAnti, amount, isNew
		return level;
	}
	
	/// <summary>
	/// Creates Level16 and returns it.
	/// </summary>
	private CLevel Level16()
	{
		CLevel level = new CLevel(12000, 3000, 5000, 6500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.HeliumNucleus, false, 1, true); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, false, 2, false);
		
		return level;
	}
	
	/// <summary>
	/// Creates Level17 and returns it.
	/// </summary>
	private CLevel Level17()
	{
		CLevel level = new CLevel(12000, 3000, 5000, 6500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.HeliumNucleus, true, 1, true); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, true, 2, false);
		
		return level;
	}
	
	/// <summary>
	/// Creates Level18 and returns it.
	/// </summary>
	private CLevel Level18()
	{
		CLevel level = new CLevel(15000, 3000, 5000, 7500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.HeliumAtom, false, 1, true); //isAnti, amount, isNew
		
		return level;
	}
	
	/// <summary>
	/// Creates Level19 and returns it.
	/// </summary>
	private CLevel Level19()
	{
		CLevel level = new CLevel(15000, 3000, 5000, 7500); //ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.HeliumAtom, true, 1, true); //isAnti, amount, isNew
		
		return level;
	}
	
	/// <summary>
	/// Creates Level20 and returns it.
	/// </summary>
	private CLevel Level20()
	{
		CLevel level = new CLevel(20000, 3000, 5000, 7500); //semi ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.CarbonNucleus, false, 1, true); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, false, 6, false);
		
		return level;
	}
	
	/// <summary>
	/// Creates Level21 and returns it.
	/// </summary>
	private CLevel Level21()
	{
		CLevel level = new CLevel(20000, 3000, 5000, 7500); //semi ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.CarbonNucleus, true, 1, true); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.Electron, true, 6, false);
		
		return level;
	}
	
	/// <summary>
	/// Creates Level22 and returns it.
	/// </summary>
	private CLevel Level22()
	{
		CLevel level = new CLevel(20000, 3000, 5000, 7000); //semi ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.CarbonAtom, false, 1, true); //isAnti, amount, isNew
		
		return level;
	}
	
	/// <summary>
	/// Creates Level23 and returns it.
	/// </summary>
	private CLevel Level23()
	{
		CLevel level = new CLevel(20000, 3000, 5000, 7000); //semi ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.CarbonAtom, true, 1, true); //isAnti, amount, isNew
		
		return level;
	}

	/// <summary>
	/// Creates Level24 and returns it.
	/// </summary>
	private CLevel Level24()
	{
		CLevel level = new CLevel(40000, 3000, 5000, 7000); //semi ok
		level.IsLocked = true;
		level.AddObjective(EItemTypes.HeliumAtom, false, 1, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.HeliumAtom, true, 1, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.CarbonAtom, false, 1, false); //isAnti, amount, isNew
		level.AddObjective(EItemTypes.CarbonAtom, true, 1, false); //isAnti, amount, isNew
		
		return level;
	}
}
