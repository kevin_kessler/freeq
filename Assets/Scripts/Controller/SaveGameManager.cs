﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using UnityEngine;



public class SaveGameManager : Singleton<SaveGameManager>  
{
	private static string saveLevelsFilePath = Application.persistentDataPath + "/savelevels.dat";
	private static string saveSettingsFilePath = Application.persistentDataPath + "/savesettings.dat";
	private bool isInsantiated;
	
	[System.Serializable]
	public class LevelData
	{
		public int ID;
		public int score;
		public bool isLocked;
		
		public LevelData(int id, int scr, bool locked){
			ID = id;
			score = scr;
			isLocked = locked;
		}
	}
	
	[System.Serializable]
	public class SettingsData
	{
		public int curLanguageIndex;
		public bool soundIsOn;
		
		public SettingsData()
		{
			curLanguageIndex = LanguageManager.CurrentLanguageIndex;
			soundIsOn = true;
		}
	}
	
	//Data to save / load
    private List<LevelData> Levels = new List<LevelData>();
	private SettingsData Settings = new SettingsData();
	
	/// <summary>
	/// Private constructor, to prevent initialisation from outside.
	/// </summary>
	private SaveGameManager(){}
	
	public void Instantiate()
	{
		if(!isInsantiated){
			Load();
			isInsantiated = true;
		}
	}
	
	/// <summary>
	/// Saves the game data to files.
	/// </summary>
	public void Save()
	{
		SaveLevels();
		SaveSettings();
	}
	
	/// <summary>
	/// Saves the settings data to file.
	/// </summary>
	public void SaveSettings()
	{
		//get settingsdata from global vars
		GetSettingsData();
		
		//get binary formatter for serialization and create a save file
		BinaryFormatter bf = new BinaryFormatter(); 
		FileStream fs = File.Create(saveSettingsFilePath);
		
		//write the data
		bf.Serialize(fs, Settings);
		fs.Close();
	}
	
	/// <summary>
	/// Gets the settings data from the respective vars and stores it in the settings object
	/// </summary>
	private void GetSettingsData()
	{
		Settings.curLanguageIndex = LanguageManager.CurrentLanguageIndex;
		Settings.soundIsOn = Globals.AudioIsOn;
	}
	
	/// <summary>
	/// Saves the level data to file.
	/// </summary>
	public void SaveLevels()
	{
		//get level data from levelmanager
		GetLevelData();
		
		//get binary formatter for serialization and create a save file
		BinaryFormatter bf = new BinaryFormatter(); 
		FileStream fs = File.Create(saveLevelsFilePath);
		
		//write the data
		bf.Serialize(fs, Levels);
		fs.Close();
	}
	
	/// <summary>
	/// Gets the level data from the levelmanager and stores it in the list
	/// </summary>
	private void GetLevelData()
	{
		Levels = new List<LevelData>();
		foreach(CLevel clevel in LevelManager.Instance.Levels){
			Levels.Add(new LevelData(clevel.ID, clevel.HighScore, clevel.IsLocked));
		}
	}
	
	
	
	/// <summary>
	/// Load game data from files and sets the loaded data to the respective objects.
	/// </summary>
	public void Load()
	{
		LoadLevels();
		LoadSettings();
	}
	
	/// <summary>
	/// Loads the settingsdata and sets it to the respective settings vars.
	/// </summary>
	public void LoadSettings()
	{
		if(File.Exists(saveSettingsFilePath))
		{
			//get binary formatter for deserialization and open the file
			BinaryFormatter bf = new BinaryFormatter(); 
			FileStream fs = null;
			try{
				fs = File.Open(saveSettingsFilePath, FileMode.Open);
				//read the data
				Settings = (SettingsData)bf.Deserialize(fs);
				fs.Close();
				
				//set the loaded data to the levels of the levelmanager
				SetSettingsData();
			}
			catch(System.Exception){
				Debug.LogWarning("Could not load Settings. Creating new Settings file");
				
				if(null != fs)
					fs.Close();
				
				File.Delete(saveSettingsFilePath);
				SaveSettings();
				LoadSettings();
				return;
			}
		}
		
		//load the language strings
		LanguageManager.Instance.LoadLanguage(LanguageManager.CurrentLanguageIndex);
	}
	
	/// <summary>
	/// Sets the loaded settings data to the respective settings vars
	/// </summary>
	private void SetSettingsData()
	{
		LanguageManager.CurrentLanguageIndex = Settings.curLanguageIndex;
		Globals.AudioIsOn = Settings.soundIsOn;
		if(!Globals.AudioIsOn)
			AudioListener.volume = 0;
	}
	
	/// <summary>
	/// Loads the leveldata and sets it to the respective levels of the levelmanager.
	/// </summary>
	public void LoadLevels()
	{
		if(File.Exists(saveLevelsFilePath))
		{
			//get binary formatter for deserialization and open the file
			BinaryFormatter bf = new BinaryFormatter(); 
			FileStream fs = null;
			try{
				fs = File.Open(saveLevelsFilePath, FileMode.Open);
				//read the data
				Levels = (List<LevelData>)bf.Deserialize(fs);
				fs.Close();
				
				//set the loaded data to the levels of the levelmanager
				SetLevelData();
			}
			catch(System.Exception){
				Debug.LogWarning("Could not load SaveGame. Creating new one");
				
				if(null != fs)
					fs.Close();
				
				File.Delete(saveLevelsFilePath);
			}
		}
	}
	
	/// <summary>
	/// Sets the level data to the levels of the levelmanager
	/// </summary>
	private void SetLevelData()
	{
		foreach(CLevel clevel in LevelManager.Instance.Levels)
		{
			foreach(LevelData dlevel in Levels)
			{
				if(clevel.ID == dlevel.ID){
					clevel.IsLocked = dlevel.isLocked;
					//clevel.IsLocked = false;          //use this to unlock all levels
					clevel.HighScore = dlevel.score;
				}
			}
		}
	}
	
}
