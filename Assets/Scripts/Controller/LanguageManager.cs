﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using UnityEngine;
using System.Text.RegularExpressions;



public class LanguageManager : Singleton<LanguageManager>  
{
	public const string LANGUAGE_ENGLISH = "English"; //must be equal to the xml file name (without extension)
	public static Texture2D ICON_ENGLISH = (Texture2D) Resources.Load("Textures/GUI/english");
	
	public const string LANGUAGE_GERMAN = "German";
	public static Texture2D ICON_GERMAN = (Texture2D) Resources.Load("Textures/GUI/german");
	
	public const string LANGUAGE_PORTUGUESE = "Portuguese";
	public static Texture2D ICON_PORTUGUESE = (Texture2D) Resources.Load("Textures/GUI/portugal");
	
	public static int CurrentLanguageIndex = 0;
	
	private List<string> mLanguages; //the filename of a language.xml must equal the language name 
	private int mCurrentLanguageIndex;
	
	public ReadOnlyCollection<string> Languages
	{
		get { return mLanguages.AsReadOnly(); }
		private set {}
	}
	
	/// <summary>
	/// Private constructor, to prevent initialisation from outside.
	/// </summary>
	private LanguageManager()
	{
		mLanguages = new List<string>();
		mLanguages.Add(LANGUAGE_ENGLISH);
		mLanguages.Add(LANGUAGE_GERMAN);
		mLanguages.Add(LANGUAGE_PORTUGUESE);
	}
	
	/// <summary>
	/// Loads the language strings from the xmlfile with the name found under the given index of the languages list.
	/// Sets those strings to the global vars
	/// </summary>
	public void LoadLanguage(int languageIndex)
	{
		if(Languages.Count > languageIndex && languageIndex >= 0)
		{
			XmlDocument langXML = LoadLanguageXML(Languages[languageIndex]);
			if(null != langXML)
			{
				CurrentLanguageIndex = languageIndex;
				
				//button strings
				Globals.STRING_LANGUAGE_BUTTON = langXML.GetElementById("LanguageButton").InnerText;
				Globals.STRING_HELP_BUTTON = langXML.GetElementById("HelpButton").InnerText;;
				Globals.STRING_AUDIO_BUTTON_ON = langXML.GetElementById("AudioButtonOn").InnerText;
				Globals.STRING_AUDIO_BUTTON_OFF = langXML.GetElementById("AudioButtonOff").InnerText;
				Globals.STRING_ABOUT_BUTTON = langXML.GetElementById("AboutButton").InnerText;
				Globals.STRING_RESET_BUTTON = langXML.GetElementById("ResetButton").InnerText;
				Globals.STRING_BACK_BUTTON = langXML.GetElementById("BackButton").InnerText;
				Globals.STRING_SETTINGS_BUTTON = langXML.GetElementById("SettingsButton").InnerText;
				Globals.STRING_PLAY_BUTTON = langXML.GetElementById("PlayButton").InnerText;
				Globals.STRING_QUIT_BUTTON = langXML.GetElementById("QuitButton").InnerText;
				Globals.STRING_YES_BUTTON = langXML.GetElementById("YesButton").InnerText;
				Globals.STRING_NO_BUTTON = langXML.GetElementById("NoButton").InnerText;
				Globals.STRING_CONFIRM_BUTTON = langXML.GetElementById("ConfirmButton").InnerText;
				Globals.STRING_PAUSE_BUTTON = langXML.GetElementById("PauseButton").InnerText;
				Globals.STRING_RESUME_BUTTON = langXML.GetElementById("ResumeButton").InnerText;
				Globals.STRING_RESTART_BUTTON = langXML.GetElementById("RestartButton").InnerText;
				Globals.STRING_LEVELSELECT_BUTTON = langXML.GetElementById("LevelSelectButton").InnerText;
				Globals.STRING_COMBINE_BUTTON = langXML.GetElementById("CombineButton").InnerText;
				Globals.STRING_RELEASE_BUTTON = langXML.GetElementById("ReleaseButton").InnerText;
				
				//dialog strings
				Globals.DIALOG_QUIT_TITLE = langXML.GetElementById("QuitDialogTitle").InnerText;;
				Globals.DIALOG_RESET_TITLE = langXML.GetElementById("ResetDialogTitle").InnerText;
				Globals.DIALOG_HELP_TITLE = langXML.GetElementById("HelpDialogTitle").InnerText;
				Globals.DIALOG_ABOUT_TITLE = langXML.GetElementById("AboutDialogTitle").InnerText;
				
				Globals.DIALOG_QUIT_TEXT = ParseStringFromXMLString( langXML.GetElementById("QuitDialogText").InnerText );
				Globals.DIALOG_RESET_TEXT = ParseStringFromXMLString( langXML.GetElementById("ResetDialogText").InnerText );
				Globals.DIALOG_HELP_TEXT = ParseStringFromXMLString( langXML.GetElementById("HelpDialogText").InnerText );
				Globals.DIALOG_ABOUT_TEXT = ParseStringFromXMLString( langXML.GetElementById("AboutDialogText").InnerText );
				
				//particle strings
				Globals.PARTICLE_CREATED_TEXT = ParseStringFromXMLString( langXML.GetElementById("ParticleCreated").InnerText );
				Globals.PARTICLE_COMBINATIONERROR_TEXT = ParseStringFromXMLString( langXML.GetElementById("CombinationError").InnerText );
				Globals.PARTICLE_NOTSELECTED_TEXT = ParseStringFromXMLString( langXML.GetElementById("NoParticlesSelected").InnerText );
				
				Globals.PARTICLE_TEXT_UQUARK = ParseStringFromXMLString( langXML.GetElementById("UpQuarkDescription").InnerText );
				Globals.PARTICLE_TEXT_DQUARK = ParseStringFromXMLString( langXML.GetElementById("DownQuarkDescription").InnerText );
				Globals.PARTICLE_TEXT_ELECTRON = ParseStringFromXMLString( langXML.GetElementById("ElectronDescription").InnerText );
				Globals.PARTICLE_TEXT_PROTON = ParseStringFromXMLString( langXML.GetElementById("ProtonDescription").InnerText );
				Globals.PARTICLE_TEXT_NEUTRON = ParseStringFromXMLString( langXML.GetElementById("NeutronDescription").InnerText );
				Globals.PARTICLE_TEXT_HELIUMNUCLEUS = ParseStringFromXMLString( langXML.GetElementById("HeliumNucleusDescription").InnerText );
				Globals.PARTICLE_TEXT_HELIUMATOM = ParseStringFromXMLString( langXML.GetElementById("HeliumAtomDescription").InnerText );
				Globals.PARTICLE_TEXT_CARBONNUCLEUS = ParseStringFromXMLString( langXML.GetElementById("CarbonNucleusDescription").InnerText );
				Globals.PARTICLE_TEXT_CARBONATOM = ParseStringFromXMLString( langXML.GetElementById("CarbonAtomDescription").InnerText );
				
				Globals.PARTICLE_TEXT_AUQUARK = ParseStringFromXMLString( langXML.GetElementById("AUpQuarkDescription").InnerText );
				Globals.PARTICLE_TEXT_ADQUARK = ParseStringFromXMLString( langXML.GetElementById("ADownQuarkDescription").InnerText );
				Globals.PARTICLE_TEXT_AELECTRON = ParseStringFromXMLString( langXML.GetElementById("AElectronDescription").InnerText );
				Globals.PARTICLE_TEXT_APROTON = ParseStringFromXMLString( langXML.GetElementById("AProtonDescription").InnerText );
				Globals.PARTICLE_TEXT_ANEUTRON = ParseStringFromXMLString( langXML.GetElementById("ANeutronDescription").InnerText );
				Globals.PARTICLE_TEXT_AHELIUMNUCLEUS = ParseStringFromXMLString( langXML.GetElementById("AHeliumNucleusDescription").InnerText );
				Globals.PARTICLE_TEXT_AHELIUMATOM = ParseStringFromXMLString( langXML.GetElementById("AHeliumAtomDescription").InnerText );
				Globals.PARTICLE_TEXT_ACARBONNUCLEUS = ParseStringFromXMLString( langXML.GetElementById("ACarbonNucleusDescription").InnerText );
				Globals.PARTICLE_TEXT_ACARBONATOM = ParseStringFromXMLString( langXML.GetElementById("ACarbonAtomDescription").InnerText );
				
				Globals.PARTICLE_TITLE_UQUARK = langXML.GetElementById("UpQuarkTitle").InnerText;
				Globals.PARTICLE_TITLE_DQUARK = langXML.GetElementById("DownQuarkTitle").InnerText;
				Globals.PARTICLE_TITLE_ELECTRON = langXML.GetElementById("ElectronTitle").InnerText;
				Globals.PARTICLE_TITLE_PROTON = langXML.GetElementById("ProtonTitle").InnerText;
				Globals.PARTICLE_TITLE_NEUTRON = langXML.GetElementById("NeutronTitle").InnerText;
				Globals.PARTICLE_TITLE_HELIUMNUCLEUS = langXML.GetElementById("HeliumNucleusTitle").InnerText;
				Globals.PARTICLE_TITLE_HELIUMATOM = langXML.GetElementById("HeliumAtomTitle").InnerText;
				Globals.PARTICLE_TITLE_CARBONNUCLEUS = langXML.GetElementById("CarbonNucleusTitle").InnerText;
				Globals.PARTICLE_TITLE_CARBONATOM = langXML.GetElementById("CarbonAtomTitle").InnerText;
				
				Globals.PARTICLE_TITLE_AUQUARK = langXML.GetElementById("AUpQuarkTitle").InnerText;
				Globals.PARTICLE_TITLE_ADQUARK = langXML.GetElementById("ADownQuarkTitle").InnerText;
				Globals.PARTICLE_TITLE_AELECTRON = langXML.GetElementById("AElectronTitle").InnerText;
				Globals.PARTICLE_TITLE_APROTON = langXML.GetElementById("AProtonTitle").InnerText;
				Globals.PARTICLE_TITLE_ANEUTRON = langXML.GetElementById("ANeutronTitle").InnerText;
				Globals.PARTICLE_TITLE_AHELIUMNUCLEUS = langXML.GetElementById("AHeliumNucleusTitle").InnerText;
				Globals.PARTICLE_TITLE_AHELIUMATOM = langXML.GetElementById("AHeliumAtomTitle").InnerText;
				Globals.PARTICLE_TITLE_ACARBONNUCLEUS = langXML.GetElementById("ACarbonNucleusTitle").InnerText;
				Globals.PARTICLE_TITLE_ACARBONATOM = langXML.GetElementById("ACarbonAtomTitle").InnerText;
				
				//complete strings
				Globals.COMPLETE_TEXT_CLEAR = langXML.GetElementById("LevelClear").InnerText;
				Globals.COMPLETE_TEXT_NOSTAR = ParseStringFromXMLString( langXML.GetElementById("NoStar").InnerText );
				Globals.COMPLETE_TEXT_1STAR = ParseStringFromXMLString( langXML.GetElementById("OneStar").InnerText );
				Globals.COMPLETE_TEXT_2STARS = ParseStringFromXMLString( langXML.GetElementById("TwoStars").InnerText );
				Globals.COMPLETE_TEXT_3STARS = ParseStringFromXMLString( langXML.GetElementById("ThreeStars").InnerText );
				Globals.COMPLETE_TEXT_HIGHSCORE = langXML.GetElementById("Highscore").InnerText;
				Globals.COMPLETE_TEXT_NEWHIGHSCORE = langXML.GetElementById("NewHighscore").InnerText;
				Globals.COMPLETE_TEXT_CONTINUE = langXML.GetElementById("Continue").InnerText;
				Globals.COMPLETE_TEXT_SKIP = langXML.GetElementById("Skip").InnerText;
				
				DialogManager.Instance.UpdateDialogStrings();
			}
		}
	}
	
	/// <summary>
	/// Loads the next language in the list
	/// </summary>
	public void LoadNextLanguage()
	{
		if(Languages.Count > 0)
		{
			int nextIndex = 0;
			if(Languages.Count > CurrentLanguageIndex+1){
				nextIndex = CurrentLanguageIndex+1;
			}
			
			LoadLanguage( nextIndex );
		}
	}
	
	/// <summary>
	/// Loads the language xml file with the given filename (without extension) and returns it.
	/// Returns null if file not found;
	/// </summary>
	public XmlDocument LoadLanguageXML(string languageFileName)
	{
		XmlDocument langXML = null;
		TextAsset textAsset = (TextAsset) Resources.Load("Languages/"+languageFileName);  
		if(null != textAsset)
		{
			langXML = new XmlDocument();
			langXML.LoadXml ( textAsset.text );
		}
		
		return langXML;
	}
	
	public Texture2D GetCurrentLanguageIcon()
	{
		if(Languages.Count > CurrentLanguageIndex && CurrentLanguageIndex >= 0)
		{
			switch(Languages[CurrentLanguageIndex])
			{
			case LANGUAGE_ENGLISH:
				return ICON_ENGLISH;
			case LANGUAGE_GERMAN:
				return ICON_GERMAN;
			case LANGUAGE_PORTUGUESE:
				return ICON_PORTUGUESE;
			default:
				return Globals.PARTICLE_TEXTURE_MISSING;
			}
		}
		else{
			return Globals.PARTICLE_TEXTURE_MISSING;
		}
	}
	
	private string ParseStringFromXMLString(string str)
	{
		//split at every newline symbol of the string
		string[] splitted = str.Split(new string[] {"\\n"}, StringSplitOptions.RemoveEmptyEntries);
		string strToReturn = "";
		
		//create new string
		for(int i=0; i<splitted.Length; i++)
		{
			//insert new line for each entry
			if(0 != i)
				strToReturn+="\n";
			
			//trim leading and tailing spaces
			splitted[i] = splitted[i].Trim(); 
			
			//apply richtext tags
			splitted[i] = splitted[i].Replace("*emph*","<b><color=#AA0000ff>");
			splitted[i] = splitted[i].Replace("*/emph*", "</color></b>");
			
			
			//concat
			strToReturn += splitted[i];
		}

		return strToReturn;
	}
}
