﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogManager : Singleton<DialogManager>  {
	
	
	
	//layout params
	private const float screenWidth = GUIResize.ORIG_SCREENWIDTH;
	private const float screenHeight = GUIResize.ORIG_SCREENHEIGHT;
	
	//rects
	private Rect confirmDialogRect;
	private Rect particleDialogRect;
	
	//styles
	private Texture2D overlayTexture;
	private Texture2D explanationTexture;
	private Texture2D logoTexture;
	
	//Dialogs
	public List<GUIDialog> Dialogs;
	public GUIDialogConfirmation ResetProgressDialog; //dialog that resets the whole game progress if confirmed
	public GUIDialogConfirmation QuitGameDialog; //dialog that quits the game if confirmed
	public GUIDialogInfo ExplainGamePlayDialog; //dialog that explains how the game is played
	public GUIDialogParticleInfo ParticleInfoDialog; //dialog which explains the UpQuark particle
	public GUIDialogInfo AboutGameDialog;
	
	//controlvars
	public bool AnyDialogIsActive = false;
	
	//private constructor to avoid init from outside
	private DialogManager(){}
	
	void Awake()
	{
		InitRects();
		ResizeRects();
		InitStyles();
		InitDialogs();
	}
	
	// Update is called once per frame
	void Update () 
	{
		AnyDialogIsActive = false;
		foreach(GUIDialog dialog in Dialogs)
		{
			if(dialog.IsActive){
				AnyDialogIsActive = true;
				break;
			}
		}
	}
	
	void OnGUI()
	{
		//overlay gui if dialog is active
		if(AnyDialogIsActive)
			GUI.DrawTexture(new Rect(0,0,Screen.width, Screen.height), overlayTexture); //overlay screen behind dialog
	}
	
	private void InitRects()
	{
		//confirm dialog rect
		float confirmDialogWidth = screenWidth * (4f/5f);
		float confirmDialogHeight = screenHeight / 2.5f;
		confirmDialogRect = new Rect(screenWidth/2 - confirmDialogWidth/2, screenHeight/2 - confirmDialogHeight/2, confirmDialogWidth, confirmDialogHeight);
		
		//particle info dialog rect
		float particleDialogWidth = screenWidth * (4f/5f);
		float particleDialogHeight = screenHeight * (9f/10f);
		particleDialogRect = new Rect(screenWidth/2 - particleDialogWidth/2, screenHeight/2 - particleDialogHeight/2, particleDialogWidth, particleDialogHeight);
	}
	
	private void ResizeRects()
	{
		confirmDialogRect = GUIResize.ResizeRect(confirmDialogRect);
		particleDialogRect = GUIResize.ResizeRect(particleDialogRect);
	}
	
	private void InitStyles()
	{
		overlayTexture = (Texture2D)Resources.Load("Textures/overlay");
		explanationTexture = (Texture2D)Resources.Load("Textures/GUI/explanation");
		logoTexture = (Texture2D)Resources.Load("Textures/GUI/cern_ml_logo");
	}
	
	private void InitDialogs()
	{
		Dialogs = new List<GUIDialog>();
		
		//confirm dialogs
		InitResetProgressDialog();
		InitQuitGameDialog();
		
		//info dialogs
		InitExplainGamePlayDialog();
		
		//particle dialog
		InitParticleInfoDialog();
		
		//about dialog
		InitAboutGameDialog();
	}
	
	public void ShowParticleDialog(EItemTypes particleType, bool isAnti, bool unPauseOnConfirm)
	{
		ParticleInfoDialog.IsAnti = isAnti;
		ParticleInfoDialog.ParticleType = particleType;
		ParticleInfoDialog.IsActive = true;
		ParticleInfoDialog.UnPauseOnConfirm = unPauseOnConfirm;
	}
	
	public void UpdateDialogStrings()
	{
		UpdateExplainGamePlayDialogText();
		UpdateQuitGameDialogText();
		UpdateResetProgressDialogText();
		UpdateAboutGameDialogText();
	}
	
	/*****************************************/
	/*****************DIALOGS*****************/
	/*****************************************/
	
	/****RESET PROGRESS DIALOG****/
	private void InitResetProgressDialog()
	{
		ResetProgressDialog = (GUIDialogConfirmation)gameObject.AddComponent("GUIDialogConfirmation");
		ResetProgressDialog.Init(confirmDialogRect);
		ResetProgressDialog.OnConfirmClick += ConfirmResetGameProgress;
		ResetProgressDialog.OnCancelClick += CancelResetGameProgress;
		ResetProgressDialog.OnCloseClick += CancelResetGameProgress;
		
		UpdateResetProgressDialogText();
		
		Dialogs.Add(ResetProgressDialog);
	}
	
	private void UpdateResetProgressDialogText()
	{
		ResetProgressDialog.Title = Globals.DIALOG_RESET_TITLE;
		ResetProgressDialog.ConfirmationMessage = Globals.DIALOG_RESET_TEXT;
	}
	
	/// <summary>
	/// call back for confirm reset game progress dialig
	/// </summary>
	private void ConfirmResetGameProgress()
	{
		LevelManager.Instance.HardResetAllLevels();
		SaveGameManager.Instance.SaveLevels();
		ResetProgressDialog.IsActive = false;
	}
	
	/// <summary>
	/// call back for cancel reset game progress dialig
	/// </summary>
	private void CancelResetGameProgress()
	{
		ResetProgressDialog.IsActive = false;
	}
	
	
	/****QUIT GAME DIALOG****/
	private void InitQuitGameDialog()
	{
		QuitGameDialog = (GUIDialogConfirmation)gameObject.AddComponent("GUIDialogConfirmation");
		QuitGameDialog.Init(confirmDialogRect);
		QuitGameDialog.OnConfirmClick += ConfirmQuitGame;
		QuitGameDialog.OnCancelClick += CancelQuitGame;
		QuitGameDialog.OnCloseClick += CancelQuitGame;
		
		UpdateQuitGameDialogText();
		
		Dialogs.Add(QuitGameDialog);
	}
	
	private void UpdateQuitGameDialogText()
	{
		QuitGameDialog.Title = Globals.DIALOG_QUIT_TITLE;
		QuitGameDialog.ConfirmationMessage = Globals.DIALOG_QUIT_TEXT;
	}
	
	/// <summary>
	/// call back for Confirm quit dialig
	/// </summary>
	private void ConfirmQuitGame()
	{
		Application.Quit();
	}
	
	/// <summary>
	/// call back for cancel and close quit dialig
	/// </summary>
	private void CancelQuitGame()
	{
		QuitGameDialog.IsActive = false;
	}
	
	/****EXPLAIN GAMEPLAY DIALOG****/
	private void InitExplainGamePlayDialog()
	{
		ExplainGamePlayDialog = (GUIDialogInfo)gameObject.AddComponent("GUIDialogInfo");
		ExplainGamePlayDialog.Init(particleDialogRect);
		ExplainGamePlayDialog.ImageTexture = explanationTexture;
		ExplainGamePlayDialog.OnConfirmClick += CloseExplainGamePlayDialog;
		ExplainGamePlayDialog.OnCloseClick += CloseExplainGamePlayDialog;
		
		UpdateExplainGamePlayDialogText();
		
		Dialogs.Add(ExplainGamePlayDialog);
	}
	
	private void UpdateExplainGamePlayDialogText()
	{
		ExplainGamePlayDialog.Title = Globals.DIALOG_HELP_TITLE;
		ExplainGamePlayDialog.Text = Globals.DIALOG_HELP_TEXT;
	}
	
	/// <summary>
	/// call back for close and confirm explain gameplay dialog
	/// </summary>
	private void CloseExplainGamePlayDialog()
	{
		ExplainGamePlayDialog.IsActive = false;
	}
	
	/****Particle Info DIALOG****/
	private void InitParticleInfoDialog()
	{
		ParticleInfoDialog = (GUIDialogParticleInfo)gameObject.AddComponent("GUIDialogParticleInfo");
		ParticleInfoDialog.Init(particleDialogRect);
		ParticleInfoDialog.OnConfirmClick += CloseParticleInfoDialog;
		ParticleInfoDialog.OnCloseClick += CloseParticleInfoDialog;
		
		Dialogs.Add(ParticleInfoDialog);
	}
	
	/// <summary>
	/// call back for close and confirm particle info dialog
	/// </summary>
	private void CloseParticleInfoDialog()
	{
		ParticleInfoDialog.IsActive = false;
	}
	
	/**** ABOUT GAME DIALOG ****/
	private void InitAboutGameDialog()
	{
		AboutGameDialog = (GUIDialogInfo)gameObject.AddComponent("GUIDialogInfo");
		AboutGameDialog.Init(particleDialogRect);
		AboutGameDialog.ImageTexture = logoTexture;
		AboutGameDialog.OnConfirmClick += CloseAboutGameDialog;
		AboutGameDialog.OnCloseClick += CloseAboutGameDialog;
		
		UpdateAboutGameDialogText();
		
		Dialogs.Add(AboutGameDialog);
	}
	
	private void UpdateAboutGameDialogText()
	{
		AboutGameDialog.Title = Globals.DIALOG_ABOUT_TITLE;
		AboutGameDialog.Text = Globals.DIALOG_ABOUT_TEXT;
	}
	
	/// <summary>
	/// call back for close and confirm about game dialog
	/// </summary>
	private void CloseAboutGameDialog()
	{
		AboutGameDialog.IsActive = false;
	}
}
