﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour 
{
	private CLevel currentLevel;
	private const int countDownSpeed = 100;
	
	// Use this for initialization
	void Start () 
	{
		currentLevel = LevelManager.Instance.CurrentLevel;
		if(null != currentLevel){
			currentLevel.CurrentScore = currentLevel.StartScore;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(null != currentLevel )
		{
			if(currentLevel.CurrentScore <= 0)
				currentLevel.CurrentScore = 0;
			else
				currentLevel.CurrentScore = currentLevel.StartScore - (int)(Time.timeSinceLevelLoad * countDownSpeed);
		}
	}
}
