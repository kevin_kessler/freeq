﻿using UnityEngine;
using System.Collections;

public class ParticleManager: MonoBehaviour {
	
	public bool isAnti; //wheter the particle is normal or antiparticle
	public EItemTypes type; //particle type (electron, uquark, dquark)
	public GameObject counterPart; //the particles counter particle (anti or normal)
	private ParticleManager counterPM; //the particlemanager of the counter particle
	public float moveSpeed = 0.5f; //the current move speed of the particle
	private float speedDistanceMultiplier = 1.25f; //multiplier for particle speed, which is depending on their distance from each other
	private float minSpeed = 0.1f; //the minimum speed a particle can have
	private float minChaseSpeed = 1.5f; //the minimum speed of a particle, when chasing its counterpart
	private float maxDistanceToCounterPart = 5f; //maximum distance between particle and counter particle
	public bool maxDistanceReached = false;
	public bool isPuttingToFreeq = false;
	public bool isDragging = false;
	public bool hasBeenDragged = false;
	private bool isShaking = false;
	private float shrinkTime = 0.75f;
	private Vector3 shrinkSize = new Vector3(0.001f, 0.001f, 0.001f);
	private Vector3 shakeAmplitude = new Vector3(0.1f,0.1f,0);
	private bool isCollided = false;
	private float radius = 0.5f;
	
	//effects
	private GameObject detonation;
	
	//anti materials
	private Material antiElecMat;
	private Material antiUQuarkMat;
	private Material antiDQuarkMat;
	
	//audio
	private AudioClip jumpSound;
	private AudioClip implosionSound;
	
	// Use this for initialization
	void Start () {
		counterPM = (ParticleManager)counterPart.GetComponent("ParticleManager");
		detonation = (GameObject)Resources.Load("Prefabs/Effects/Detonator-Simple");
		antiElecMat = (Material)Resources.Load("Materials/AElectronMat");
		antiDQuarkMat = (Material)Resources.Load("Materials/ADQuarkMat");
		antiUQuarkMat = (Material)Resources.Load("Materials/AUQuarkMat");
		
		radius = ((SphereCollider)collider).radius * transform.localScale.x;
		
		if(isAnti){
			ApplyAntiTexture();
		}
		
		jumpSound = (AudioClip)Resources.Load("Audio/collect");
		implosionSound = (AudioClip)Resources.Load("Audio/implosion");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!isCollided)
		{
			//both alive?
			if(null != counterPart)
			{
				//as long as particle and counterparticle are not going to the freeq, handle their interaction with each other
				if(!isPuttingToFreeq && !counterPM.isPuttingToFreeq){
					HandleParticlePair();
				}
				
				//if counter particle goes to freeq, shake and destroy the particle (as long as this particle is not going to freeq)
				else if(counterPM.isPuttingToFreeq && !isPuttingToFreeq){
					Shake();
				}
			}
			
			//counterpart does not exist anymore, so detonate the particle
			else{
				if(!isDragging && !isPuttingToFreeq)
					Detonate();
			}
		}
	}
	
	/// <summary>
	/// Handles the particle pair.
	/// If they collide, the collision effect starts, if not they move around
	/// </summary>
	private void HandleParticlePair()
	{
		//check distance
		bool collided = PointsAreClose(radius*2, gameObject.transform.position, counterPart.transform.position);

		if(collided){
			//start particle collision effect
			ParticleCollision();
		}
		else{
			//move particles around
			MoveParticles();
		}
	}
	
	/// <summary>
	/// Moves the particles back and forth
	/// </summary>
	private void MoveParticles()
	{
		//calc distance and check if maxDistance is reached
		float distanceToCounterPart = Vector3.Distance(transform.position, counterPart.transform.position);
		if(distanceToCounterPart > maxDistanceToCounterPart){
			maxDistanceReached = true;
			counterPM.maxDistanceReached = true;
		}
		
		//calc movement speed depending on the distance of particle and counterparticle
		moveSpeed = (maxDistanceToCounterPart - distanceToCounterPart)*speedDistanceMultiplier;
		
		//prevent getting too slow
		if(moveSpeed < minSpeed)
			moveSpeed = minSpeed;
		
		//if counterpart is dragged, chase it with at least minChaseSpeed
		if((counterPM.hasBeenDragged || hasBeenDragged) && (moveSpeed < minChaseSpeed) )
			moveSpeed = minChaseSpeed;
			
		
		//move apart or together, depending on the distance and dragging state
		Vector3 repulseDirection = transform.position - counterPart.transform.position;
		if(!maxDistanceReached && !counterPM.hasBeenDragged){
			transform.Translate(repulseDirection * moveSpeed * Time.deltaTime, Space.World);
		}
		else{
			transform.Translate(-repulseDirection * moveSpeed * Time.deltaTime, Space.World);
		}
	}
	
	/// <summary>
	/// Is called when the particle collides with its counterpart.
	/// Sets isCollided bools of both and starts Absorb effect of both.
	/// </summary>
	private void ParticleCollision()
	{
		isCollided = true;
		counterPM.isCollided = true;
		
		//they collided, so start absorb effect
		Vector3 collisionPoint = (gameObject.transform.position + counterPart.transform.position) / 2f; //calc collision point
		StartCoroutine(Absorb( collisionPoint ));
		StartCoroutine(counterPM.Absorb( collisionPoint ));
	}
	
	/// <summary>
	/// Is Called on Left MouseButton Down
	/// Starts DragParticle() Coroutine.
	/// </summary>
	void OnMouseDown(){
		if(!Globals.IsPause && !isCollided)
			StartCoroutine(DragParticle());
	}
	
	/// <summary>
	/// Changes the game objects 3D position according to the 2D mouseposition
	/// If dragging to the freeq, it moves inside.
	/// </summary>
	private IEnumerator DragParticle()
	{
		isDragging = true;
		hasBeenDragged = true;
		
		//move gameobject accoring to mouse position
		Vector3 particleScreenPos = Camera.main.WorldToScreenPoint(transform.position);
	    Vector3 offsetToMouse = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, particleScreenPos.z));
	    while (Input.GetMouseButton(0))
	    {
			//stop dragging if collided with counterpart
			if(isCollided){
				break;
			}
		
	       	Vector3 currentMousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, particleScreenPos.z);
	       	Vector3 newParticlePos = Camera.main.ScreenToWorldPoint(currentMousePos) + offsetToMouse;
	       	transform.position = newParticlePos;
	       	yield return null;
	    }
		
		//trigger on release after drag
		OnParticleRelease();
		
		isDragging = false;
	}
	
	/// <summary>
	/// Is called when DragParticle() gets released.
	/// </summary>
	private void OnParticleRelease()
	{
		//move to freeq if particle released in freeq drop zone
		if(IsInDropZone() && !isCollided && !LevelManager.Instance.CurrentLevel.Freeq.IsFull()){
			isPuttingToFreeq = true;
			StartCoroutine(Shrink());
			MoveToFreeq();
		}
	}
	
	/// <summary>
	/// Determines whether this particle is in the freeq drop zone or not.
	/// </summary>
	private bool IsInDropZone()
	{
		//check if released in freeq drop zone
		Vector3 particleScreenPos = Camera.main.WorldToScreenPoint(transform.position);
		particleScreenPos.y = Screen.height - particleScreenPos.y; //inverse y, cause ScreenPoint y != gui y
		return GUIGamePlayFreeqBar.dropZone.Contains(particleScreenPos);
	}
	
	/// <summary>
	/// Creates a structure particle of the respective type and adds it to the freeq.
	/// </summary>
	private void MoveToFreeq()
	{
		CItem particle = null;
		switch(type){
		case EItemTypes.UQuark:
			particle = new CElementaryParticle(EItemTypes.UQuark, isAnti);
			break;
		case EItemTypes.DQuark:
			particle = new CElementaryParticle(EItemTypes.DQuark, isAnti);
			break;
		case EItemTypes.Electron:
			particle = new CElementaryParticle(EItemTypes.Electron, isAnti);
			break;
		}
		
		if(null != particle){
			LevelManager.Instance.CurrentLevel.Freeq.AddItem(particle);
			AudioSource.PlayClipAtPoint(jumpSound, Camera.main.transform.position, 0.3f);
		}
	}
	
	/// <summary>
	/// Shrinks the particle gameobject and destroys it after.
	/// </summary>
	private IEnumerator Shrink()
	{	
		//remove collider for performance reasons and for preventing dragability
		SphereCollider myCollider = (SphereCollider)gameObject.GetComponent("SphereCollider");
		Destroy(myCollider);
		
		//shrink the particle
		iTween.ScaleTo(gameObject, shrinkSize, shrinkTime);
		yield return new WaitForSeconds(shrinkTime);
		Destroy(gameObject);
	}
	
	/// <summary>
	/// moves the particle to the given collisionpoint, and puts a detonation animation in front. then destroys the particles.
	/// </summary>
	public IEnumerator Absorb(Vector3 collisionPoint)
	{
		if(isAnti) //play explosion only once (not for both particles)
		{
			Vector3 detoPoint = new Vector3(collisionPoint.x, collisionPoint.y, (collisionPoint.z-1));
			Instantiate(detonation, detoPoint, transform.rotation);
			AudioSource.PlayClipAtPoint(implosionSound, Camera.main.transform.position, 0.1f);
		}
		
		//remove collider for performance reasons and for preventing dragability
		SphereCollider myCollider = (SphereCollider)gameObject.GetComponent("SphereCollider");
		Destroy(myCollider);
		
		//move particle to the collisionPoint to merge with counter particle
		iTween.MoveTo(gameObject, collisionPoint, moveSpeed/(20f*speedDistanceMultiplier));
		
		//wait till particle and counterparticle are merged
		while( !PointsAreClose(0.025f, gameObject.transform.position, counterPart.transform.position) ){
			yield return null;
		}

		Destroy(gameObject);
	}
	
	/// <summary>
	/// Shakes the particle from left to right
	/// ShakeTime is depending on the shrinktime for particles that move to the freezer, cause after counterpart went to the freezer, the shaking part dies.
	/// </summary>
	private void Shake()
	{
		if(!isShaking){
			iTween.ShakePosition(gameObject, shakeAmplitude, shrinkTime);
			isShaking = true;
		}
		else if(isDragging){
			iTween.Stop(gameObject);
		}
		else if(!isPuttingToFreeq){
			isShaking = false;
		}
	}
	
	/// <summary>
	/// Shows the Detonate Effect and destroys the game object
	/// </summary>
	private void Detonate()
	{
		Instantiate(detonation, transform.position, transform.rotation);
		AudioSource.PlayClipAtPoint(implosionSound, Camera.main.transform.position, 0.1f);
		Destroy(gameObject);
	}
	
	
	
	
	/****HELPER FUNCTIONS****/
	
	/// <summary>
	/// Checks if the distance of the two given points is less than the given distancevalue
	/// </summary>
	private bool PointsAreClose(float dist, Vector3 pointA, Vector3 pointB)
	{
		if( Vector3.Distance(pointA, pointB) < dist)
			return true;
		else
			return false;
	}
	
	/// <summary>
	/// Applies the respective anti texture to the particle 
	/// </summary>
	private void ApplyAntiTexture()
	{
		switch(type){
		case EItemTypes.DQuark:
			renderer.material = antiDQuarkMat;
			break;
		case EItemTypes.UQuark:
			renderer.material = antiUQuarkMat;
			break;
		case EItemTypes.Electron:
			renderer.material = antiElecMat;
			break;
		}
	}
}