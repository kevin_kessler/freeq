﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {
	
	private enum SpawnType : int
	{
		None = -1,
		UQuark = 0,
		DQuark = 1,
		Electron = 2
	}
	
	private static float spawnFrequencyMin = 0.75f; //mintime in seconds between spawns
	private static float spawnFrequencyMax = 1.5f; //maxtime in seconds between spawns
	private static float spawnTimer;
	private const float zSpawnMax = 15;
	private static float curZSpawn = zSpawnMax;
	private static GameObject dQuarkPrefab;
	private static GameObject uQuarkPrefab;
	private static GameObject electronPrefab;
	private CLevel currentLevel;
	
	// Use this for initialization
	void Start () 
	{
		spawnTimer = 0;
		dQuarkPrefab = (GameObject)Resources.Load("Prefabs/Particles/DQuark");
		uQuarkPrefab = (GameObject)Resources.Load("Prefabs/Particles/UQuark");
		electronPrefab = (GameObject)Resources.Load("Prefabs/Particles/Electron");
		currentLevel = LevelManager.Instance.CurrentLevel;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(null != currentLevel && !currentLevel.Freeq.IsOpened)
		{
			if(spawnTimer > 0){
			  spawnTimer -= Time.deltaTime;
			}
			if(spawnTimer <= 0){
				SpawnRandomParticles();
				spawnTimer = Random.Range (spawnFrequencyMin, spawnFrequencyMax);
			}
		}
	}
	
	/// <summary>
	/// Spawns two particles (normal and anti) of a random type.
	/// </summary>
	private void SpawnRandomParticles()
	{
		//move spawn manager to random pos rot
		transform.position = GenerateRandomSpawnVector();
		//transform.rotation = Random.rotation;
		
		//make random choice of which type to spawn (depending on which types are allowed to spawn)
		SpawnType randomType = GetRandomSpawnType();
			
		//set prefab according to the random choice
		GameObject prefabType = null;
		if(randomType == SpawnType.UQuark){
			prefabType = uQuarkPrefab;
		}
		else if(randomType == SpawnType.DQuark){
			prefabType = dQuarkPrefab;
		}
		else if(randomType == SpawnType.Electron){
			prefabType = electronPrefab;
		}
		else{
			Debug.LogWarning("Elementary Particles are not allowed to spawn in this level");
		}
		
		//spawn normal particle and anti particle
		if(null != prefabType)
			SpawnParticles(prefabType);
	}
	
	/// <summary>
	/// Instantiates particle and antiparticle at the SpawnManagers position and rotation (by the given prefab).
	/// Sets its ParticleManagers isAnti bool and counterPart
	/// </summary>
	/// <returns> The created gameobject (particle)</returns>
	private GameObject[] SpawnParticles(GameObject prefab)
	{
		GameObject[] particles = new GameObject[2];
		
		//spawn particles
		GameObject particle = (GameObject)Instantiate(prefab, transform.position, transform.rotation);
		Vector3 offsetVector = GetOffsetSpawnVector(particle); //avoid spawning at the same position
		GameObject antiparticle = (GameObject)Instantiate(prefab, offsetVector, transform.rotation);
		
		//set particle manager values
		//Vector3 moveDirection = GetCounterDirection2D(particle, antiparticle);
		
		//particle
		ParticleManager pm = (ParticleManager)particle.GetComponent("ParticleManager");
		pm.isAnti = false;
		pm.counterPart = antiparticle;
		//pm.moveDirection = moveDirection;
		
		//antiparticle
		ParticleManager apm = (ParticleManager)antiparticle.GetComponent("ParticleManager");
		apm.isAnti = true;
		apm.counterPart = particle;
		//apm.moveDirection = -moveDirection;
		
		particles[0] = particle;
		particles[1] = antiparticle;
		return particles;
	}
	
	/// <summary>
	/// Generates a random vector3 in world coordinates within the cameras view.
	/// </summary>
	private static Vector3 GenerateRandomSpawnVector()
	{
		Vector3 randomVector = new Vector3(0,0,0);
		
		//viewport coordinates: 0,0 = bottom left / 1,1 = upper right
		randomVector.x = Random.Range(0.4f, 0.6f); 
		randomVector.y = Random.Range(0.5f, 0.6f);
		randomVector = Camera.main.ViewportToWorldPoint(randomVector); //transform coordinates from viewport to 3d
		
		//prevent collision with other particles
		randomVector.z = curZSpawn;
		curZSpawn -= 1.5f;
		if(curZSpawn <= 0f) curZSpawn = zSpawnMax;
		
		return randomVector;
	}
	
	/// <summary>
	/// Calculates a point which lies on a circle around the given particle.
	/// The radius of that circle is 2*radius of given particle (+0.1 cause of floating precision issue)
	/// </summary>
	private static Vector3 GetOffsetSpawnVector(GameObject particle)
	{
		//get center and radius of the particle
		float radius = ((SphereCollider)particle.collider).radius * particle.transform.localScale.x;
		Vector2 center2D = new Vector2(particle.transform.position.x , particle.transform.position.y);
		
		//find a random point close to the given particle, but far enough to spawn the antiparticle without colliding
		float randomAngle = Random.Range(0,360) * Mathf.Deg2Rad;
		float x = center2D.x + Mathf.Cos(randomAngle)*2.1f*radius;
		float y = center2D.y + Mathf.Sin(randomAngle)*2.1f*radius;
		Vector2 newPoint = new Vector2(x, y);
		//thus, the new point is always 2*r+0.5 far away from the center of the given particle.
		
		return new Vector3(newPoint.x, newPoint.y, particle.transform.position.z);
	}
	
	/// <summary>
	/// Gets the counter direction of a particles antiparticle in 2D space
	/// </summary>
	private static Vector3 GetCounterDirection2D(GameObject particle, GameObject antiparticle)
	{
		Vector3 directionToAnti = antiparticle.transform.position - particle.transform.position; 
		directionToAnti.z = 0;
		return -directionToAnti;
	}
	
	/// <summary>
	/// Makes random choice of which type to spawn (depending on which types are allowed to spawn), and returns the type.
	/// 'None' if no one allowed
	/// </summary>
	/// <returns>
	/// The random spawn type.
	/// </returns>
	private SpawnType GetRandomSpawnType()
	{
		SpawnType randomType = SpawnType.None;
		if(currentLevel.SpawnsDQuarks && currentLevel.SpawnsUQuarks && currentLevel.SpawnsElectrons){
			randomType = (SpawnType)Random.Range((int)SpawnType.UQuark, (int)SpawnType.Electron + 1); //returns 0,1,2
		}
		else if(currentLevel.SpawnsDQuarks && currentLevel.SpawnsUQuarks){
			randomType = (SpawnType)Random.Range((int)SpawnType.UQuark, (int)SpawnType.DQuark + 1); //returns 0,1
		}
		else if(currentLevel.SpawnsDQuarks && currentLevel.SpawnsElectrons){
			randomType = (SpawnType)Random.Range((int)SpawnType.DQuark, (int)SpawnType.Electron + 1); //returns 1,2
		}
		else if(currentLevel.SpawnsUQuarks && currentLevel.SpawnsElectrons){
			randomType = (SpawnType)Random.Range((int)SpawnType.UQuark, (int)SpawnType.DQuark + 1); //returns 0,1
			if(randomType == SpawnType.DQuark) randomType = SpawnType.Electron; //avoid DQuark, cause not allowed
		}
		else if(currentLevel.SpawnsUQuarks){
			randomType = SpawnType.UQuark;
		}
		else if(currentLevel.SpawnsDQuarks){
			randomType = SpawnType.DQuark;
		}
		else if(currentLevel.SpawnsElectrons){
			randomType = SpawnType.Electron;
		}
		
		//minor occurance of electrons
		if(randomType == SpawnType.Electron)
		{
			int changeIt = Random.Range(0,2);

			if(1 == changeIt)
			{
				if(currentLevel.SpawnsUQuarks)
				{
					int toUQuark = Random.Range(0,2);
					if(1 == toUQuark || !currentLevel.SpawnsDQuarks)
						randomType = SpawnType.UQuark;
					else
						randomType = SpawnType.DQuark;
				}
				else if(currentLevel.SpawnsDQuarks){
					randomType = SpawnType.DQuark;
				}
			}
		}
		
		return randomType;
	}
}
