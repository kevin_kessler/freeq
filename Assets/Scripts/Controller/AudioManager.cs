﻿using UnityEngine;
using System.Collections;

public class AudioManager : Singleton<AudioManager> {
	
	public static AudioClip ButtonSound = (AudioClip)Resources.Load("Audio/button");
	public static AudioClip MenuMusic = (AudioClip)Resources.Load("Audio/menumusic");
	public static AudioClip GameplayMusic = (AudioClip)Resources.Load("Audio/gameplaymusic");
	
	public static void SelfInit()
	{
		Instance.InitMusicObject();
	}
	
	private static bool isInited = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	private AudioManager()
	{

	}
	
	//static init
	private void InitMusicObject()
	{
		if(!isInited)
		{
			gameObject.AddComponent("AudioSource");
			gameObject.audio.loop = true;
			PlayMenuMusic();
			
			isInited = true;
		}
	}
	
	public static void PlayButtonSound()
	{
		if(Globals.IsPause)
			Time.timeScale = 1; //used to play button sounds on pause as well
		
		AudioSource.PlayClipAtPoint(ButtonSound, Camera.main.transform.position);
		
		if(Globals.IsPause)
			Time.timeScale = 0;
	}
	
	public void PlayGamePlayMusic()
	{
		gameObject.audio.Stop();
		gameObject.audio.clip = GameplayMusic;
		gameObject.audio.Play();
	}
	
	public void PlayMenuMusic()
	{
		gameObject.audio.Stop();
		gameObject.audio.clip = MenuMusic;
		gameObject.audio.Play();
	}
}
